<?
/* @var $this DefaultController */

$this->breadcrumbs=array(
	Yii::t('breadcrumbs', 'monitor') => array('/monitor/frontend'),
//	Yii::app()->seo->h1,
);
?>
<? if($server['status']): ?>
	<figure>
		<? if(file_exists(Yii::getPathOfAlias('webroot').'/images/maps/'.$server['map'].'.jpg')):?>
			<img class="img" alt="<?=Yii::t('server', 'map')?>  - <?=$server['map']?>" title="<?=Yii::t('server', 'map')?> - <?=$server['map']?>" src="<?=Yii::app()->request->baseUrl?>/images/maps/<?=$server['map']?>.jpg">
		<? else: ?>	
			<img class="img" src="<?=Yii::app()->request->baseUrl?>/images/maps/nomap.jpg">	
		<? endif; ?>
		<figcaption>
			<a href="steam://connect/<?=$server['address']?>:<?=$server['port']?>"><?=$server['address']?>:<?=$server['port']?></a>
		</figcaption>
	</figure>    
	<script  src="/js/hidden/hidden-content.js"></script>
	<div id="players">
		<!--<img alt="loader" title="loader" class="ajax-img" src="/images/frontend/loading.gif">-->
		<div id="ajax-content"></div>
	</div>
	
<? else : ?>
	<h3>Server is offline</h3>
<? endif; ?>
<div class="clear"></div>


