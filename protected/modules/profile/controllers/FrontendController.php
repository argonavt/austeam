<?php
class FrontendController extends FController
{
	public function actionIndex(){
		$steamID64 = Yii::app()->request->getParam('userid');
		$steamID32 = UsersConnects::getSteamId32($steamID64);

		$data = array(
			'steamID64' => $steamID64,
			'steamID' => '',
			'skype' => '',
			'AUS' => 0,
			'accessToEditing' => false,
			'playerName' => '',
			'admin' => false,
			'banCount' => 0,
			'lastVisit' => '',
			'mostActiveServerName' => '',
			'mostActiveServerID' => 0,
			'status' => 'offline',
			'ip' => Yii::app()->params['domain'],
			'servId' => '',
			'port' => '',
			'JoinServerName' => '',
			'showBanCounts' => false,
			'avatarFull' => '/images/frontend/no_image.jpg',
			'is_steam' => false
		);

		if($steamID64 === UsersConnects::getSteamId64()){
			$data['accessToEditing'] = true;
		}		
		
		$players = new Users;		
		$user = $players->getPrivateUserData($steamID32);
		
		if($user){
			if($user['noSteamID'] && $user['noSteamID'] !== $user['steamID'] && $steamID32 !== $user['steamID']){ 
				$this->redirect(Yii::app()->createUrl('/profile/'.UsersConnects::getSteamId64($user['steamID'])));
			}elseif($user['noSteamID'] && $user['noSteamID'] !== $user['noSteamID']){
				$steamID32 = $user['noSteamID'];
			}	
			$data['skype'] = $user['skype'];
			$data['AUS'] = $user['AUS'];	
		}
		
		$data['steamID'] = $steamID32;
		
		$userGameData = $players->getUserGameData($steamID32);
		
		if($userGameData){
			$data['playerName'] = $userGameData['playerName'];
			if($userGameData['playerName'] == $userGameData['admin'] && $userGameData['admin']){
				$data['admin'] = true;
			}
			
			$data['banCount'] = $userGameData['banCount'];
			$data['showBanCounts'] = UsersConnects::checkAdminRights();
			
			if('0000-00-00 00:00:00' !== $userGameData['lastConnect'] && $userGameData['lastConnect']){
				if(Yii::app()->language == 'en'){
					$data['lastVisit'] = Date::getFormatDate($userGameData['lastConnect'], 'd/m/Y'); 
				}else{
					$data['lastVisit'] = Date::getFormatDate($userGameData['lastConnect'], 'd.m.Y');
				}	
			}
			if($userGameData['serverName'] == 'Public'){
				$data['mostActiveServerName'] = "{$userGameData['serverName']} - {$userGameData['map']}";
			}else{
				$data['mostActiveServerName'] = "{$userGameData['serverName']}";
			}
			$data['mostActiveServerID'] = "{$userGameData['serverId']}";			
		}
		
		$userOnlineData = $players->getUserOnlineData($steamID32);
		if($userOnlineData){
			$data['status'] = 'online';
			$data['servId'] = $userOnlineData['serverId'];
			$data['port'] = $userOnlineData['port'];
			$data['JoinServerName'] = $userOnlineData['name'];			
		}
		if($steam = $players->getSteamData($steamID64)){	
			if(isset($steam['avatarFull'])  && $steam['avatarFull'] !== 'http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_full.jpg'){
				$data['avatarFull'] = $steam['avatarFull'];			
			}
			if(!$data['playerName'] && isset($steam['playerName']) && $steam['playerName']){
				$data['playerName'] = $steam['playerName'];
			}	
			$data['is_steam'] = true;
		}

		
		if(!$data['playerName']){
			$data['playerName'] = 'Anonymous';
		}
		
		if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('_short', $data);
			die;
		}
		
		
		Yii::app()->seo->setTitle($data['playerName']);	
		
		$this->render('index', $data);
	}

	public function actionEdit(){
		$steamID64 = UsersConnects::getSteamId64();
		$steamID32 = UsersConnects::getSteamId32($steamID64);
		
		$data = array(
			'steamID64' => $steamID64,
			'steamID' => '',
			'skype' => '',
			'playerName' => '',
			'avatarFull' => '/images/frontend/no_image.jpg',
			'changeSuccess' => false,
			'model',
		);	
		// redirects
		if(!Yii::app()->request->cookies['openid_sig']){
			$this->redirect(Yii::app()->createUrl('/profile/'.$steamID64));
		}	
		if(Yii::app()->request->getParam('userid') !== $steamID64){
			if($steamID64){
				$this->redirect(Yii::app()->createUrl('/profile/'.$steamID64.'/edit'));
			}else{
				$this->redirect('/');
			}
		}
		$user = Users::model()->find(array(
				'condition'=>'steamID=:steamID',
				'params'=>array(':steamID'=>$steamID32),
		));
		if(!$user){
			$this->redirect(Yii::app()->createUrl('steam/frontend/logout'));
		}
		
		if(Yii::app()->request->isPostRequest){
			$formData = Yii::app()->request->getParam('Users');
			
			if($formData['noSteamID']){
				$user['noSteamID'] = strtoupper($formData['noSteamID']);
			}
			if($formData['skype']){
				$user['skype'] = $formData['skype'];
			}			
			if($formData['mail']){
				$user['mail'] = $formData['mail'];
			}			
			if($user->validate()){
				$user->isNewRecord = false;
				$user->save();
				
				$data['changeSuccess'] = true;
			}
		}		
		
		$data['model'] = $user;
		
		$players = new Users;
		
		$userGameData = $players->getUserGameData($steamID32);
		if($userGameData){
			$data['playerName'] = $userGameData['playerName'];	
		}
		
		$steam = $players->getSteamData($steamID64);
		
		if(isset($steam['avatarFull'])  && $steam['avatarFull'] !== 'http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_full.jpg'){
			$data['avatarFull'] = $steam['avatarFull'];			
		}	
		if(!$data['playerName']){
			$data['playerName'] = $steam['playerName'];
		}		
		
		Yii::app()->seo->setTitle($data['playerName']);	
		
		$this->render('edit', $data);
	}		
}