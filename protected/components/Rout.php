<?php
Yii::import('application.modules.seo.models.*');
class Rout extends CBaseUrlRule
{
	private $_params;
 
    public function createUrl($manager,$route,$params,$ampersand)
    {
		$cacheHash = 'url_'.$route.'_'.serialize($params);
		
		if(Yii::app()->cache->get($cacheHash)){
			if('empty' == Yii::app()->cache->get($cacheHash)){
				return false;
			}
			return Yii::app()->cache->get($cacheHash);
		}
		
		$result = Yii::app()->db->createCommand()
			->select('url, seo.alias')
			->from('urls')
			->join('seo', 'seo.rout = urls.id')
			->queryAll();	
		
		$modificatedParams = array();
		foreach($params as $param => $value){
			$modificatedParams[$param] = (string)$value;
		}
		
		
		$urls = array();
		foreach($result as $url){
			if($url['alias']){
				$urls[$url['url']] = $url['alias'];
			}
		}
		
		$myRout = isset($route) ? $route.(!empty($modificatedParams) ? '|'.serialize($modificatedParams) : ''): '';
	
		if(isset($urls[$myRout])){
			Yii::app()->cache->set($cacheHash, $urls[$myRout], 604800);
			return $urls[$myRout];
		}

		Yii::app()->cache->set($cacheHash, 'empty', 604800);
        return false;  // не применяем данное правило
    }
 
    public function parseUrl($manager,$request,$pathInfo,$rawPathInfo)
    {		
		$lang = current(explode('.', Yii::app()->request->getServerName()));
		if(key_exists($lang, Yii::app()->params['translatedLanguages'])){
			Yii::app()->language = $lang;
		}else{
			Yii::app()->language = 'en';
		}
		if(empty($pathInfo)){
			return false;
		}
		
		foreach( $manager->rules as  $pattern => $route){		
			if(is_array($route)){
				continue;
			}
			$rule = new CUrlRule($route,$pattern);
			if(preg_match($rule->pattern, $pathInfo.'/', $match)){
				$currentRoute = $rule->route;
				$currentParams = array();
				foreach($rule->params as $param => $value){
					$currentParams[$param] = $match[$param];
				}
				
				break;
			}	
		}
		
		$rout = isset($currentRoute) ? $currentRoute.(!empty($currentParams) ? '|'.serialize($currentParams) : ''): '';

		$seo = Yii::app()->db->createCommand()
			->select('url, seo.alias')
			->from('urls')
			->join('seo', 'seo.rout = urls.id')
			->where('alias = :alias OR url = :url', array(':alias' => $pathInfo, ':url' => $rout))
			->queryRow();
		
		if(isset($seo['alias']) && $seo['alias'] !== $pathInfo){		
			header('Location: /'.$seo['alias'], true, 301);
			die;
		}

		if(isset($seo['url']) && $seo['url']){		
			$path = explode('|', $seo['url']);
			$this->_params = unserialize(ltrim($path[1], '|'));
			$_GET = $this->_params;
			return $path[0];
		}
        return false;  // не применяем данное правило
    }
}

