<?php

/**
 * This is the model class for table "seo".
 *
 * The followings are the available columns in table 'seo':
 * @property string $id
 * @property string $rout
 * @property string $title
 * @property string $desc
 * @property string $h1
 * @property string $text
 * @property string $alias
 * @property string $redirect
 * @property integer $notFound
 * @property string $canonical
 * @property integer $index
 * @property integer $follow
 *
 * The followings are the available model relations:
 * @property Urls $rout0
 * @property Urls $canonical0
 * @property Urls $redirect0
 */
class Seo extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'seo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('rout', 'required'),
            array('notFound, index, follow', 'numerical', 'integerOnly'=>true),
            array('rout, redirect, canonical', 'length', 'max'=>10),
            array('title, h1, alias', 'length', 'max'=>75),
            array('desc', 'length', 'max'=>255),
            array('text', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, rout, title, desc, h1, text, alias, redirect, notFound, canonical, index, follow', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rout0' => array(self::BELONGS_TO, 'Urls', 'rout'),
            'canonical0' => array(self::BELONGS_TO, 'Urls', 'canonical'),
            'redirect0' => array(self::BELONGS_TO, 'Urls', 'redirect'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'rout' => 'Rout',
            'title' => 'Title',
            'desc' => 'Desc',
            'h1' => 'H1',
            'text' => 'Text',
            'alias' => 'Alias',
            'redirect' => 'Redirect',
            'notFound' => 'Not Found',
            'canonical' => 'Canonical',
            'index' => 'Index',
            'follow' => 'Follow',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('rout',$this->rout,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('desc',$this->desc,true);
        $criteria->compare('h1',$this->h1,true);
        $criteria->compare('text',$this->text,true);
        $criteria->compare('alias',$this->alias,true);
        $criteria->compare('redirect',$this->redirect,true);
        $criteria->compare('notFound',$this->notFound);
        $criteria->compare('canonical',$this->canonical,true);
        $criteria->compare('index',$this->index);
        $criteria->compare('follow',$this->follow);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Seo the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}