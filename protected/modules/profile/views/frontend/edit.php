<div class="user_page">
	<div class="left_part">
        <img src="<?=$avatarFull?>">
		<a class="change_ava" href="http://steamcommunity.com/profiles/<?=$steamID64?>/edit"><?=Yii::t('profile', 'Change an Avatar')?></a>
    </div>
    <div class="center_part">
		<h1><?=$playerName?></h1>
		<?=CHtml::beginForm(); ?>
		<table>	
			<tr>
				<td><?=CHtml::activeLabel($model, 'noSteamID'); ?></td>
				<td><?=CHtml::activeTextField($model, 'noSteamID'); ?><td>
			</tr>
			<tr>
				<td><?=CHtml::activeLabel($model, 'skype'); ?></td>
				<td><?php echo CHtml::activeTextField($model, 'skype'); ?><td>			
			</tr>
			<tr>
				<td><?=CHtml::activeLabel($model, 'mail'); ?></td>
				<td><?=CHtml::activeTextField($model, 'mail'); ?><td>					
			</tr>

		</table>
		<?=CHtml::submitButton(Yii::t('profile', 'Save')); ?>
		<a class="steam_id" href="<?=Yii::app()->createUrl('/profile/'.$steamID64)?>"><?=Yii::t('profile', 'Return back')?></a>
		<?=CHtml::endForm(); ?>	
		<p><?=CHtml::errorSummary($model); ?></p>	
		
		<? if(isset($changeSuccess) && $changeSuccess == true): ?>	
			<p class="success"><?=Yii::t('profile', 'Changes have been saved')?></p>
		<? endif; ?>			
		
    </div>
    
</div>
