<?php

/**
 * This is the model class for table "comments".
 *
 * The followings are the available columns in table 'comments':
 * @property string $id
 * @property string $parent
 * @property string $page
 * @property string $lang
 * @property string $user_id
 * @property string $comments
 * @property integer $like
 * @property string $date
 */
class Comments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page, lang, comments, like, date', 'required'),
			array('like', 'numerical', 'integerOnly'=>true),
			array('parent, page, user_id', 'length', 'max'=>10),
			array('lang', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent, page, lang, user_id, comments, like, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent' => 'Parent',
			'page' => 'Page',
			'lang' => 'Lang',
			'user_id' => 'User',
			'comments' => 'Comments',
			'like' => 'Like',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('parent',$this->parent,true);
		$criteria->compare('page',$this->page,true);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('like',$this->like);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getComments($page = 'self'){
		$commetns = Yii::app()->db->createCommand()
			->from('comments')
			->leftJoin('urls url', 'url.id = page')
			->where('lang = :lang AND url = :url', array(':lang'=> Yii::app()->language, ':url' => SeoController::getCurrentUriPath()))			
			->queryAll();	
		
//		var_dump($comments);
		
		
		return $commetns;
	}
	
	
}
