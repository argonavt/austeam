<?php

class sms extends Payment
{
	private $controller;
	
	
	public function __construct($controller) {
		$this->controller = $controller;
	}
	
	public function index(){	
		Yii::import("application.extensions.SMSBill.SMSBill");
		$data = array(
			'error' => '',
			'smsForm' => '',		
		);
				
		$steamID = Yii::app()->session->get("steamID");
		
		$smsbill = new SMSBill();
		switch(Yii::app()->language){
			case 'ru' : $smsbill->setServiceId(11941);	break;
			default :  $smsbill->setServiceId(11918); //изменить на свое ID доступное в Личном кабинете
		}
		
		$smsbill->useEncoding('utf-8');
		$smsbill->useHeader('no');
		$smsbill->useJQuery('yes');
		$smsbill->useLang(Yii::app()->language);
		$smsbill->useCss('http://'.Yii::app()->params['domain'].'/css/payment.css');
		
		$smsbill_password = Yii::app()->request->getPost('smsbill_password');

		if ($smsbill_password) {
			if (!$smsbill->checkPassword($smsbill_password)) {
				$data['error'] =  Yii::t('vip', 'Password is incorrect, go back and try again');
			}else{ 
				//введен верный пароль
				//выполняются алгоритмы в случае верного пароля
				if(Yii::app()->request->isPostRequest){
					$this->addVip($steamID);
					
					$vips = new Vips;
					$vip = $vips->getInfo($steamID);
					
					switch(Yii::app()->language){
						case 'ru' : $data['vipEnd'] = date('d.m.Y | G:i', $vip['expires']);	break;
						default :  $data['vipEnd'] = date('d/m/Y | g.i a', $vip['expires']); 
					}		
					
					$this->controller->render('success', $data);
					return;
				}else{
					$data['error'] =  Yii::t('vip', 'Error, try again');
				}
			}
		}else{
			//показать платежную форму т.к. пароль еще не был введен
			$data['smsForm'] = $smsbill->getForm();
			
			$data['prevUlr'] = Yii::app()->createUrl(Yii::app()->controller->module->id.'/payment');
		}	
		$this->controller->render('/payments/'.Yii::app()->request->getQuery('payment').'/index', $data);		
	}
}
