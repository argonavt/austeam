<?php

/**
 * This is the model class for table "rankme".
 *
 * The followings are the available columns in table 'rankme':
 * @property integer $id
 * @property string $steam
 * @property string $name
 * @property string $lastip
 * @property string $score
 * @property string $kills
 * @property string $deaths
 * @property string $suicides
 * @property string $tk
 * @property string $shots
 * @property string $hits
 * @property string $headshots
 * @property string $connected
 * @property string $rounds_tr
 * @property string $rounds_ct
 * @property string $lastconnect
 * @property string $knife
 * @property string $glock
 * @property string $usp
 * @property string $p228
 * @property string $deagle
 * @property string $elite
 * @property string $fiveseven
 * @property string $m3
 * @property string $xm1014
 * @property string $mac10
 * @property string $tmp
 * @property string $mp5navy
 * @property string $ump45
 * @property string $p90
 * @property string $galil
 * @property string $ak47
 * @property string $sg550
 * @property string $famas
 * @property string $m4a1
 * @property string $aug
 * @property string $scout
 * @property string $sg552
 * @property string $awp
 * @property string $g3sg1
 * @property string $m249
 * @property string $hegrenade
 * @property string $flashbang
 * @property string $smokegrenade
 * @property string $head
 * @property string $chest
 * @property string $stomach
 * @property string $left_arm
 * @property string $right_arm
 * @property string $left_leg
 * @property string $right_leg
 * @property string $c4_planted
 * @property string $c4_exploded
 * @property string $c4_defused
 * @property string $ct_win
 * @property string $tr_win
 * @property string $hostages_rescued
 * @property string $vip_killed
 * @property string $vip_escaped
 * @property string $vip_played
 */
class Rank extends CActiveRecord
{
	public $lastName;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'rankme';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('score, kills, deaths, suicides, tk, shots, hits, headshots, connected, rounds_tr, rounds_ct, lastconnect, knife, glock, usp, p228, deagle, elite, fiveseven, m3, xm1014, mac10, tmp, mp5navy, ump45, p90, galil, ak47, sg550, famas, m4a1, aug, scout, sg552, awp, g3sg1, m249, hegrenade, flashbang, smokegrenade, head, chest, stomach, left_arm, right_arm, left_leg, right_leg, c4_planted, c4_exploded, c4_defused, ct_win, tr_win, hostages_rescued, vip_killed, vip_escaped, vip_played', 'length', 'max'=>10),
            array('steam, name, lastip', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, steam, name, lastip, score, kills, deaths, suicides, tk, shots, hits, headshots, connected, rounds_tr, rounds_ct, lastconnect, knife, glock, usp, p228, deagle, elite, fiveseven, m3, xm1014, mac10, tmp, mp5navy, ump45, p90, galil, ak47, sg550, famas, m4a1, aug, scout, sg552, awp, g3sg1, m249, hegrenade, flashbang, smokegrenade, head, chest, stomach, left_arm, right_arm, left_leg, right_leg, c4_planted, c4_exploded, c4_defused, ct_win, tr_win, hostages_rescued, vip_killed, vip_escaped, vip_played', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'steam' => 'Steam',
            'name' => 'Name',
            'lastip' => 'Lastip',
            'score' => 'Score',
            'kills' => 'Kills',
            'deaths' => 'Deaths',
            'suicides' => 'Suicides',
            'tk' => 'Tk',
            'shots' => 'Shots',
            'hits' => 'Hits',
            'headshots' => 'Headshots',
            'connected' => 'Connected',
            'rounds_tr' => 'Rounds Tr',
            'rounds_ct' => 'Rounds Ct',
            'lastconnect' => 'Lastconnect',
            'knife' => 'Knife',
            'glock' => 'Glock',
            'usp' => 'Usp',
            'p228' => 'P228',
            'deagle' => 'Deagle',
            'elite' => 'Elite',
            'fiveseven' => 'Fiveseven',
            'm3' => 'M3',
            'xm1014' => 'Xm1014',
            'mac10' => 'Mac10',
            'tmp' => 'Tmp',
            'mp5navy' => 'Mp5navy',
            'ump45' => 'Ump45',
            'p90' => 'P90',
            'galil' => 'Galil',
            'ak47' => 'Ak47',
            'sg550' => 'Sg550',
            'famas' => 'Famas',
            'm4a1' => 'M4a1',
            'aug' => 'Aug',
            'scout' => 'Scout',
            'sg552' => 'Sg552',
            'awp' => 'Awp',
            'g3sg1' => 'G3sg1',
            'm249' => 'M249',
            'hegrenade' => 'Hegrenade',
            'flashbang' => 'Flashbang',
            'smokegrenade' => 'Smokegrenade',
            'head' => 'Head',
            'chest' => 'Chest',
            'stomach' => 'Stomach',
            'left_arm' => 'Left Arm',
            'right_arm' => 'Right Arm',
            'left_leg' => 'Left Leg',
            'right_leg' => 'Right Leg',
            'c4_planted' => 'C4 Planted',
            'c4_exploded' => 'C4 Exploded',
            'c4_defused' => 'C4 Defused',
            'ct_win' => 'Ct Win',
            'tr_win' => 'Tr Win',
            'hostages_rescued' => 'Hostages Rescued',
            'vip_killed' => 'Vip Killed',
            'vip_escaped' => 'Vip Escaped',
            'vip_played' => 'Vip Played',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('steam',$this->steam,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('lastip',$this->lastip,true);
        $criteria->compare('score',$this->score,true);
        $criteria->compare('kills',$this->kills,true);
        $criteria->compare('deaths',$this->deaths,true);
        $criteria->compare('suicides',$this->suicides,true);
        $criteria->compare('tk',$this->tk,true);
        $criteria->compare('shots',$this->shots,true);
        $criteria->compare('hits',$this->hits,true);
        $criteria->compare('headshots',$this->headshots,true);
        $criteria->compare('connected',$this->connected,true);
        $criteria->compare('rounds_tr',$this->rounds_tr,true);
        $criteria->compare('rounds_ct',$this->rounds_ct,true);
        $criteria->compare('lastconnect',$this->lastconnect,true);
        $criteria->compare('knife',$this->knife,true);
        $criteria->compare('glock',$this->glock,true);
        $criteria->compare('usp',$this->usp,true);
        $criteria->compare('p228',$this->p228,true);
        $criteria->compare('deagle',$this->deagle,true);
        $criteria->compare('elite',$this->elite,true);
        $criteria->compare('fiveseven',$this->fiveseven,true);
        $criteria->compare('m3',$this->m3,true);
        $criteria->compare('xm1014',$this->xm1014,true);
        $criteria->compare('mac10',$this->mac10,true);
        $criteria->compare('tmp',$this->tmp,true);
        $criteria->compare('mp5navy',$this->mp5navy,true);
        $criteria->compare('ump45',$this->ump45,true);
        $criteria->compare('p90',$this->p90,true);
        $criteria->compare('galil',$this->galil,true);
        $criteria->compare('ak47',$this->ak47,true);
        $criteria->compare('sg550',$this->sg550,true);
        $criteria->compare('famas',$this->famas,true);
        $criteria->compare('m4a1',$this->m4a1,true);
        $criteria->compare('aug',$this->aug,true);
        $criteria->compare('scout',$this->scout,true);
        $criteria->compare('sg552',$this->sg552,true);
        $criteria->compare('awp',$this->awp,true);
        $criteria->compare('g3sg1',$this->g3sg1,true);
        $criteria->compare('m249',$this->m249,true);
        $criteria->compare('hegrenade',$this->hegrenade,true);
        $criteria->compare('flashbang',$this->flashbang,true);
        $criteria->compare('smokegrenade',$this->smokegrenade,true);
        $criteria->compare('head',$this->head,true);
        $criteria->compare('chest',$this->chest,true);
        $criteria->compare('stomach',$this->stomach,true);
        $criteria->compare('left_arm',$this->left_arm,true);
        $criteria->compare('right_arm',$this->right_arm,true);
        $criteria->compare('left_leg',$this->left_leg,true);
        $criteria->compare('right_leg',$this->right_leg,true);
        $criteria->compare('c4_planted',$this->c4_planted,true);
        $criteria->compare('c4_exploded',$this->c4_exploded,true);
        $criteria->compare('c4_defused',$this->c4_defused,true);
        $criteria->compare('ct_win',$this->ct_win,true);
        $criteria->compare('tr_win',$this->tr_win,true);
        $criteria->compare('hostages_rescued',$this->hostages_rescued,true);
        $criteria->compare('vip_killed',$this->vip_killed,true);
        $criteria->compare('vip_escaped',$this->vip_escaped,true);
        $criteria->compare('vip_played',$this->vip_played,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Rank the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}