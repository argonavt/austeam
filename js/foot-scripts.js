//function main_init() {
	if(!$('#aside-left div').length){
		$("#aside-left").detach();
		$('article').css('margin-left', '0');
	}

	$("head").append("<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,800' rel='stylesheet' type='text/css'>");
	

	for(var i=0; i < initFunctions.length; ++i){
		initFunctions[i]();
	}
	$('.nano').nanoScroller();
//}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-53599031-1', 'auto');
ga('send', 'pageview');