<?php
class FrontendController extends FController
{

	public function actionIndex() {
		$params = array();
		if(Yii::app()->request->isAjaxRequest){
			$result = Yii::app()->db->createCommand()
				->select('name, length, created, ends, adm.user')
				->from('sb_bans b')
				->join('sb_admins adm', 'adm.aid = b.aid')
				->order('created DESC')
				->limit('50')
				->queryAll();		
			$bans = array();

			foreach($result as $ban){

				$ban['createdDate'] = date("M d", $ban['created']);
				$ban['createdTime'] = date("G:i", $ban['created']);
				$ban['endDate'] = date("M d", $ban['ends']);
				$ban['endTime'] = date("G:i", $ban['ends']);			 
				$bans[] = $ban;
			} 

			$params['bans'] = $bans;
			$this->renderPartial('_ajaxBanslist',$params);
			Yii::app()->end();
		}	
		$this->render('index', $params);
	}

}
