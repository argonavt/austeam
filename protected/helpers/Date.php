<?php
/**
 * Description of Date
 *
 * @author argonavt
 */
class Date 
{
	public static function getFormatDate($dataTime, $format){ 
		$date = new DateTime($dataTime);
		return $date->format($format);
	}
}
