<div class="pay_pages">
    <h1><?=Yii::t('vip', 'Confirm your account')?>:</h1>
    <script type="text/javascript">
        initFunctions[initFunctions.length] = function(){
            $.ajax({
                type: "POST",   
                url: "<?=Yii::app()->createUrl('profile/'.$steamID)?>",
                success: function(result){
                    if(!result){
                        result = "<div class='error'><?=Yii::t('vip', 'User is not found !')?></div>";
                    }
                    $("#short-profile").append(result);
                    $(".ajax-img").hide();
                },error:function(){
                    $("#short-profile").append("<div class='error'><?=Yii::t('vip', 'User is not found !')?></div>");
                }
            });
        }
    </script>
    <div id="short-profile">
        <img alt="loader" title="loader" class="ajax-img" src="/images/frontend/loading.gif">
    </div>
    <div class="back"><a href="<?=$prevUlr?>"><?=Yii::t('vip', 'Back'); ?></a></div>
    <div class="next"><a href="<?=$nextUlr?>"><?=Yii::t('vip', 'Next'); ?></a></div>
</div>