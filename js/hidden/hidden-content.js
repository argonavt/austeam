initFunctions[initFunctions.length] = function(){
    $.ajax({
        type: "POST",   
        url: window.location.href,
        success: function(html){
            $("#ajax-content").parent().find('.ajax-img').hide();
            $("#ajax-content").append(html);
        }
    });
}