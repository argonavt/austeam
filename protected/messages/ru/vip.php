<?php
return array(
	'Password is incorrect, go back and try again' => 'Введенный пароль не верный, вернитесь назад и попробуйте еще раз',
	'SMS payment' => 'SMS платёж',
	'Buying VIP account' => 'Покупка VIP аккаунта',
	'User is not found !' => 'Пользователь не найден !',
	'Enter your Steam ID' => 'Введите ваш Steam ID',
	'Next' => 'Далее',
	'Back' => 'Назад',
	'(You can check your Steam ID, type <em>!steamid</em> in the game)' => '(Что бы узнать ваш Steam ID, введите <em>!steamid</em> в игре)',
	'Confirm your account' => 'Подтвердите ваш аккаунт',
	'Select payment system' => 'Выберите платежную систему',
	'VIP account was successfully added' => 'VIP аккаунт успешно добавлен',
	'Changes will be available in 30 minutes (after map reloading).' => 'Изменения вступят в силу, в течении 30 минут (после перезагрузки карты).',
	'Your VIP account will be available up to' => 'Ваш VIP аккаунт действителен до',
 );
