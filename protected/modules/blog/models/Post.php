<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property string $id
 * @property string $name
 * @property string $annotation
 * @property string $post
 * @property string $date
 * @property integer $reviews
 * @property string $categories_id
 * @property integer $public
 * @property string $img
 * @property string $lang_id
 *
 * The followings are the available model relations:
 * @property Languages $lang
 * @property Category $categories
 */
class Post extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'post';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, categories_id', 'required'),
            array('reviews, public', 'numerical', 'integerOnly'=>true),
            array('name, img', 'length', 'max'=>45),
            array('categories_id, lang_id', 'length', 'max'=>10),
            array('annotation, post, date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, annotation, post, date, reviews, categories_id, public, img, lang_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lang' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
            'categories' => array(self::BELONGS_TO, 'Category', 'categories_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'annotation' => 'Annotation',
            'post' => 'Post',
            'date' => 'Date',
            'reviews' => 'Reviews',
            'categories_id' => 'Categories',
            'public' => 'Public',
            'img' => 'Img',
            'lang_id' => 'Lang',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('annotation',$this->annotation,true);
        $criteria->compare('post',$this->post,true);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('reviews',$this->reviews);
        $criteria->compare('categories_id',$this->categories_id,true);
        $criteria->compare('public',$this->public);
        $criteria->compare('img',$this->img,true);
        $criteria->compare('lang_id',$this->lang_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Post the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
	
	public function getPosts(){
		$results = Yii::app()->db->createCommand()
			->select('p.id, p.annotation, p.date, p.reviews, p.img, p.name')
			->from($this->tableName().' p')
			->join('languages l', 'l.id = lang_id')
			->where('public = :public AND l.short_name = :lang', array(':public'=>1, ':lang'=> Yii::app()->language))
			->order('date DESC')
			->limit('20')
			->queryAll();	
		$posts = array();
		foreach($results as $post){				
			if(Yii::app()->language == 'en'){
				$post['time'] = Date::getFormatDate($post['date'], 'g.i a');
				$post['date'] = Date::getFormatDate($post['date'], 'd/m/Y'); 
			}else{
				$post['time'] = Date::getFormatDate($post['date'], 'G:i');
				$post['date'] = Date::getFormatDate($post['date'], 'd.m.Y'); 
			}
			
			$posts[] = $post;
		}			
		return $posts;		
	}

	public function getPost($id){
		$post = Yii::app()->db->createCommand()
			->from($this->tableName())
			->where('id = :id', array(':id'=>$id))
			->queryRow();
		$post['time'] = Date::getFormatDate($post['date'], 'g:i a'); 
		$post['date'] = Date::getFormatDate($post['date'], 'd.m.Y'); 

		Yii::app()->db->createCommand()->update(
			$this->tableName(), 
			array(
				'reviews'=> ++$post['reviews'],
			), 
			'id = :id', 
			array(':id'=>$id)
		);		
		
		
		return $post;
	}	
}