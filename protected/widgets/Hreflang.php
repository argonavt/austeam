<?php
class Hreflang extends CWidget
{
	public function run() {
		$params = array();
		$params['langs'] = array();
	
		$url = '/';
		
		switch(Yii::app()->controller->getRoute()){
			case 'page/frontend/index': $url = ''; break;
			case 'blog/frontend/deteil': $url = ''; break;
			default : $url = Yii::app()->request->url;
		}		
		
		if(!$url){
			if(Yii::app()->language == 'en'){
				$hreflang = Yii::app()->language;
			}else{
				$hreflang = Yii::app()->language.'-'. strtoupper(Yii::app()->language);
			}
			
			$params['langs'][Yii::app()->language]['url'] = 'http://'.Yii::app()->params['domain'].Yii::app()->request->url;
			$params['langs'][Yii::app()->language]['hreflang'] = $hreflang;
		}else{
			foreach(Yii::app()->params['translatedLanguages'] as $shortLang => $lang){
				if($shortLang == 'en'){
					$domain = 'http://'.Yii::app()->params['domain'];	
				}else{
					$domain =  'http://'.$shortLang.'.'.Yii::app()->params['domain'];
				}	

				if($shortLang == 'en'){
					$hreflang = $shortLang;
				}else{
					$hreflang = $shortLang.'-'. strtoupper($shortLang);
				}
				$params['langs'][$shortLang]['url'] = $domain.$url;
				$params['langs'][$shortLang]['hreflang'] = $hreflang;
			}
		}
		$this->render('application.views.site.hreflang', $params);
	}	
}