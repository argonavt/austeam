<?php
class FrontendController extends FController
{

	public function actionIndex() {
		$params = array();
		$posts = new Post();
		$params['posts'] = $posts->getPosts();
	
		$this->render('index', $params);
	}
	
	public function actionDeteil() {
		$params = array();
		$postId = Yii::app()->request->getQuery('post');
		$post = new Post();
		$params['post'] = $post->getPost($postId);

		Yii::app()->seo->setH1($params['post']['name']);

		$this->render('deteil', $params);
	}
}
