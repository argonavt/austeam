<?php
Yii::import('comments.models.*');
class PageComments extends CWidget
{
	public function run() {
		$params = array();
		
		$model = new Comments;
		
		$comments = $model->getComments();

		$this->render('application.modules.comments.views.frontend.widget-comments', $params);
	}	
}

