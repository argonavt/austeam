<?php
require_once('Openid/Openid.php');
class FrontendController extends FController
{
	public function actionIndex()
	{
		$openid = new LightOpenID(str_replace('http://', '', Yii::app()->request->hostInfo));	
		if($openid->mode && $openid->validate() && Yii::app()->request->getParam('openid_sig')) {
			
			$idData = explode('/', Yii::app()->request->getParam('openid_claimed_id'));
			$steamId64 = end($idData);
			
			$steam_id32 = UsersConnects::getSteamId32($steamId64);		
			
			$cookie = new CHttpCookie('openid_sig', Yii::app()->request->getParam('openid_sig'), array('domain' => '.'.Yii::app()->params['domain']));		
			$cookie->expire = time()+60*60*24*30;
			Yii::app()->request->cookies['openid_sig'] = $cookie;

			$objDateTime = new DateTime;
			
			$user = new UsersConnects;
			$user->steam_id = $steam_id32;		
			$user->eventTime = $objDateTime->format('Y-m-d H:i:s');
			$user->ip = Yii::app()->request->userHostAddress;
			$user->user_hash = md5(Yii::app()->request->userHostAddress.Yii::app()->request->userAgent);
			$user->openid_sig = Yii::app()->request->getParam('openid_sig');
			$user->countrycode = '00';
			$user->save();	

			
			$userExists = Yii::app()->db->createCommand()
				->select('count(*) as rows')
				->from('users')	
				->where('steamID=:steamID', array(':steamID'=> $steam_id32))
				->queryRow();			

			
			
			
			if(!$userExists || $userExists['rows'] == 0){
				$profile = new Users;
				$profile->steamID = $steam_id32;
				$profile->noSteamID = '';
				$profile->skype = '';
				$profile->mail = '';
				$profile->AUS = 0;
				$profile->save();
			}
			
			$this->render('windowClose');		
		}else{
			echo 'fail';
		}
		
		
	}
	
	public function actionLogin()
	{
		$openid = new LightOpenID(str_replace('http://', '', Yii::app()->request->hostInfo));
		if(!$openid->mode) {
			$openid->identity = 'http://steamcommunity.com/openid';
			$openid->returnUrl = Yii::app()->createAbsoluteUrl(Yii::app()->createUrl('steam/frontend/index/'));			
			
			$this->redirect( $openid->authUrl());				
		}elseif(Yii::app()->request->urlReferrer){
			$this->redirect(Yii::app()->request->urlReferrer);
		}else{
			$this->redirect('/');
		}
	}	

	public function actionLogout()
	{
		if(isset(Yii::app()->request->cookies['openid_sig'])){
			setcookie("openid_sig", '', 0, '/', '.'.Yii::app()->params['domain']);
		}
		if(Yii::app()->request->urlReferrer){
			$uri = Yii::app()->request->urlReferrer;
			$uri = str_replace('/edit', '', $uri);
			$this->redirect($uri);
		}else{
			$this->redirect('/');
		}
	}	
	
	public function actionRegistration()
	{
		$this->redirect('https://store.steampowered.com/join/');
	}	
	
	
	
}