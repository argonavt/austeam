<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Austeam',
	'defaultController'=>'blog/frontend',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*'
	),

	'modules'=>array(
		'page' => array(),
		'bans' => array(), 
		'seo' => array(),
		'monitor' => array(),
		'stats' => array(),
		'blog' => array(),
		'steam' => array(),
		'profile' => array(),
		'search' => array(),
		'vip' => array(),
		'vip_test' => array(),
		'comments' => array(),
        'updater' => array(),
	),

	// application components
	'components'=>array(
		
        'clientScript'=>array(
            'class'=>'application.extensions.ExtendedClientScript.ExtendedClientScript',
            'combineCss'=>true,
            'compressCss'=>true,
            'combineJs'=>true,
            'compressJs'=>true,
			'autoRefresh' => false,
        ),		
		
		'seo' => array(
			'class'=>'application.components.SeoController',
		),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		
		'rout' => array(
			'class'=>'application.components.Rout',
		),
		
        'cache'=>array(
            'class'=>'system.caching.CMemCache',
            'servers'=>array(
                array('host'=>'localhost', 'port'=>11211, 'weight'=>60),
            ),			
			
        ),	
		'session' => array(
			'cookieParams' => array('domain' => '.austeam.net'),
		),	

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'useStrictParsing' => true,
			'caseSensitive' => true,
			'urlSuffix' => '',
			'rules'=>array(
				/*
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				 */
				'<alias:\w+>' => array(
					'class' => 'application.components.Rout',
				),					
				''=>'blog/frontend',				
				'gii'=>													'gii',
				'gii/<controller:\w+>'=>								'gii/<controller>',
				'gii/<controller:\w+>/<action:\w+>'=>					'gii/<controller>/<action>', 
				
				'monitor/server/<server:\d+>'=>							'monitor/frontend/deteil',
				'post/<post:\d+>'=>										'blog/frontend/deteil',
				'page/<page:\d+>'=>										'page/frontend',		
				
				'steam/<from>'=>										'steam/frontend/index/',
				'steam/frontend/<action:\w+>/'=>						'steam/frontend/<action>/',
				
				'profile/<userid:\d+>' =>								'profile/frontend/index/userid/<userid>',
				'profile/short' =>										'profile/frontend/short',
				'profile/<userid:\d+>/edit' =>							'profile/frontend/edit/userid/<userid>',
				
				'vip_test/payment/<payment:\w+>'=>						'vip_test/frontend/showpayment/action/index',				
				'vip_test/payment/<payment:\w+>/<action:\w+>'=>			'vip_test/payment/<payment>/<action>',
				'vip_test/<action:\w+>' =>								'vip_test/frontend/<action>',
				'vip_test' =>											'vip_test/frontend/index',
				
				'vip-guest/step/<step:\d+>' =>							'vip/guest/step/step/<step>',
				'vip-guest/step/<step:\w+>' =>							'vip/guest/step/step/<step>',
				'vip-guest' =>											'vip/guest/index',
				'vip/step/<step:\d+>' =>								'vip/logged/step/step/<step>',
				'vip' =>												'vip/logged/index',
				
				'<module:\w+>'=>										'<module>/frontend',
                '<module:\w+>/frontend/<action:\w+>'=>					'<module>/frontend/<action>',
				
			),
		),
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=css',             
			'emulatePrepare' => true,
			'username' => 'css',
			'password' => 'DCehBEXQXs7nGYWY',
			'charset' => 'utf8',
			'initSQLs'=>array('set names utf8'),	
			
			// включаем профайлер
			'enableProfiling'=>true,
			// показываем значения параметров
			'enableParamLogging' => true,			
			
		),		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		
		'log'=>array(
			'class'=>'CLogRouter',
			'enabled'=>YII_DEBUG,
			'routes'=>array(
				#...
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				array(
					'class'=>'application.extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
					'ipFilters'=>array('127.0.0.1','176.37.93.104'),
				),
			),
		),	
		
		'curl' => array(
			'class' => 'application.extensions.Curl.Curl',
			'options' => array(/* additional curl options */),
		),		
		
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'argonavt@i.ua',
		'domain' => 'austeam.net',
		'ip' => '94.244.136.175',
		'rcon' => 'plhzujdrf',
        'translatedLanguages'=>array(
            'en'=>'English',			
            'ru'=>'Russian',
        ),		
	),
);
