<?php
class Blocks extends CWidget
{
	public function run() {
		if(!Servers::getBarStatus()){
			return;
		}	
		

		$params['blocks'] = array();
		
		$IPs = array(
			'37.143.11.15',
			'144.76.5.215'.
			'104.28.21.10',
			'178.162.192.220',
			'185.25.116.34',
			'104.28.18.125',
			'104.28.31.114',
//			'176.37.93.104',
			'104.28.20.10 ',
			'193.33.170.249'
		);		
		
		if(in_array(Yii::app()->request->userHostAddress, $IPs)){
			$params['blocks'][] = '
				<a href="http://gungame.biz/masterserver-boost.html">CS Boost</a>	
				<a href="http://cs-monitor.su" target="_blank">мониторинг кс</a>
				<a href="http://go-cs.ru/" target="_blank">Сервера CS 1.6</a>
				<a href="http://cs-servera.net/" target="_blank">Сервера КС</a>
				<a href=\'http://monitoring.ukraina-in-cs.com.ua/\' target=\'_blank\'>Игровые сервера cs 1.6</a>
				<a href="http://my-cs.ru/" target="_blank">Сервера CS 1.6</a>
				<a href="http://sky-tracker.net/">Sky-Tracker.net</a>
				<a href="http://ip-games.ru/s/1607" target="_blank"><img src="http://xml.ip-games.ru/img/88x31.jpg" alt="Мониторинг игровых серверов - IP-Games.ru"></a>
				<a href="http://cs-master.ru" target="_blank">CS 1.6 сервера</a>
			';
		}
		
		switch(Yii::app()->language){
			case 'ru' :
				$params['blocks'][] = '
					<a href="'.Yii::app()->createUrl('/page/frontend', array('page' => '4')).'"><img alt="админ" src="/images/pages/4/admin.jpg" width="245"></a>
				';				
				break;
			default :
				$params['blocks'][] = '
					<a href="'.Yii::app()->createUrl('/page/frontend', array('page' => '3')).'"><img alt="admin" src="/images/pages/3/admin_en.jpg" width="245"></a>
				';					
		}
	
		$this->render('application.modules.html-blocks.views.frontend.index', $params);
	}	
}