<?php
require_once('Steam/steam-condenser.php');
class FrontendController extends FController
{
	public function actionIndex() {
		
		// Получаем список серверов
		$servers = Servers::model()->findAll(array(
			'select'=>'name, address, port, serverId, addressAlias',
			'order' => 'name'
		));

		$params['servers'] = array();
		foreach($servers as $key => $serverInfo){
			$server = new CServerInfo($serverInfo->address, $serverInfo->port);
			$info = $server->getInfo();	
			$params['servers'][$serverInfo->serverId]['port'] = $serverInfo->port;
			$params['servers'][$serverInfo->serverId]['address'] = $serverInfo->addressAlias ? $serverInfo->addressAlias : $serverInfo->address;			
			if($info){
				$params['servers'][$serverInfo->serverId]['status'] = true;
				$params['servers'][$serverInfo->serverId]['name'] = $serverInfo->name;
				$params['servers'][$serverInfo->serverId]['map'] = $info['map'];
				$params['servers'][$serverInfo->serverId]['maxPlayers'] = $info['maxplayers'];
				$params['servers'][$serverInfo->serverId]['numberOfPlayers'] = $info['numplayers'];
			}else{
				$params['servers'][$serverInfo->serverId]['name'] = $serverInfo->name;
				$params['servers'][$serverInfo->serverId]['status'] = false;				
			}
		}	
		
		Servers::hideBar();
		
		$this->render('index', $params);
	}
	
	
	public function actionDeteil() {	
		$serverId = Yii::app()->request->getQuery('server');	
		$server = Servers::model()->find(array(
			'select'=>'name, address, port, rcon_password, serverId, addressAlias',
			'condition'=>'serverId=:serverId',
			'params'=>array(':serverId'=> (int)$serverId)			
		));	
		
		$serverSocket = new CServerInfo($server->address, $server->port);
		$info = $serverSocket->getInfo();
		if($info){
			$players = $serverSocket->getPlayers();
			$params['server'] = array();	
			$params['server']['status'] = true;
			$params['server']['name'] = $server->name;
			$params['server']['address'] = $server->addressAlias ? $server->addressAlias : $server->address;
			$params['server']['port'] = $server->port;
			$params['server']['map'] = $info['map'];
			$params['server']['maxPlayers'] = $info['maxplayers'];
			$params['server']['players'] = array();
			
			
			if(Yii::app()->request->isAjaxRequest){
			// получаем список игроков
				
				$params['server']['showBanCounts'] = UsersConnects::checkAdminRights();
				
				if(Yii::app()->cache->get('server_'.$serverId.'_players') !== false){
					$playersData = Yii::app()->cache->get('server_'.$serverId.'_players');
				}else{					
					$subquery = Yii::app()->db->createCommand()
						->select('authid, count(*) AS banCount')
						->from('sb_bans')
						->where('length >= 604800')
						->group('authid')
						->text;

					$playersData = Yii::app()->db->createCommand()
						->select('names.name ,uniq.uniqueId, adm.user, bans.banCount')
						->from('hlstats_PlayerNames names')
						->leftJoin('hlstats_PlayerUniqueIds uniq', 'uniq.playerId = names.playerId')
						->leftJoin('hlstats_Events_PlayerAttackedPlayer atac', 'uniq.playerId = atac.playerId')	
						->leftJoin('sb_admins adm', 'uniq.uniqueId = adm.authid')
						->leftJoin('('.$subquery.') bans', 'bans.authid = uniq.uniqueId')
						->where('atac.serverId = :serverID AND atac.eventTime >= date_sub(now(), interval 15 MINUTE) AND names.lastuse >= date_sub(now(), interval 30 MINUTE)', array(':serverID' => (int)$serverId))
						->group('uniq.uniqueId, names.name')
						->order('names.lastuse DESC')
						->queryAll();	
					Yii::app()->cache->set('server_'.$serverId.'_players', $playersData, 60);
				}			
				
				foreach($players as $player){
					$params['server']['players'][$player['index']]['connectTime'] =  $player['time'];					
					$params['server']['players'][$player['index']]['name'] = $player['name'];
					$params['server']['players'][$player['index']]['score'] = $player['kills'];	
					$params['server']['players'][$player['index']]['admin'] = false;
					foreach($playersData as $playerData){
						if($playerData['name'] == $player['name']){
							$params['server']['players'][$player['index']]['uniqueId'] = UsersConnects::getSteamId64($playerData['uniqueId']);
							if($playerData['user'] == $player['name']){
								$params['server']['players'][$player['index']]['admin'] = true;
							}
							
							if($playerData['banCount']){
								$params['server']['players'][$player['index']]['banCount'] = $playerData['banCount'];
							}								
							break;
						}
					}
				}	
				$this->renderPartial('_ajaxPlyerList',$params);
				Yii::app()->end();			
			}
			
			
		
			Yii::app()->seo->add2Variable('address', $params['server']['address'].':'.$params['server']['port']);
			
		}else{
			$params['server']['status'] = false;
		}
		$this->render('deteil', $params);
	}
}
