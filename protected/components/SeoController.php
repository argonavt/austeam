<?php
/**
 * Компонент, в котором сосредоточены все методы, реализующие Seo возможности
 */
Yii::import('application.modules.seo.models.*');
class SeoController extends CComponent {
	
	private $title = '';
	private $description = '';
	private $h1 = '';
	private $text = '';
	private $index;
	private $follow;
	private $templateVariables = array();
	public function init(){

		if(strpos(Yii::app()->request->url, '/index.php') === 0){
			Yii::app()->controller->redirect('/', true, 301);
		}
		
		$criteria = new CDbCriteria;	
		$criteria->select = '`title`,`desc`, `h1`, `text`, `index`, `follow`';
		$criteria->join = 'LEFT JOIN urls ON rout = `urls`.`id` '.
					      'LEFT JOIN languages ON languages.id = t.lang_id';
		$criteria->condition = '`urls`.`url` = :url AND short_name = :lang';
		$criteria->params = array(':url'=> SeoController::getCurrentUriPath(), ':lang' => Yii::app()->language);	
	
		$seo = Seo::model()->find($criteria);

		if(isset($seo->title)){
			$this->title = $seo->title;
		}
		if(isset($seo->desc)){
			$this->description = $seo->desc;
		}
		if(isset($seo->h1)){
			$this->h1 = $seo->h1;
		}
		
		if(isset($seo->text)){
			$this->text = $seo->text;
		}
		if(isset($seo->index)){
			$this->index = $seo->index;
		}	
		if(isset($seo->follow)){
			$this->follow = $seo->follow;
		}	
		
		$this->add2Variable('name', $this->h1);

	}
	
	public function add2Variable($key, $value){
		$this->templateVariables[$key] = $value;
	}
	
	
	private function getType(){
		$rout = Yii::app()->urlManager->parseUrl(Yii::app()->request);
		$lang = '';
		if(Yii::app()->language !== 'en'){
			$lang = '_'.Yii::app()->language;
		}		
		switch($rout){
			case 'monitor/frontend/deteil':
				return  'servers'.$lang;
			case 'blog/frontend/deteil':
				$this->add2Variable('name', $this->h1);
				Yii::app()->seo->setRobots(1, 1);
				return  'newsPost'.$lang;		
		}
	}
	
	
	private function metaData($type, $tag, $params = array()){	
		switch($type){
			case 'servers':	
				$data = array(
					"title" => "{$params['name']} no-steam server — international css portal | ".Yii::app()->name,
					"desc" => "Free {$params['name']} Counter-Strike Source server, there are many popular maps, plagins and other. Connect to {$params['address']} ☕ ",
					"h1" => "",					
				);
				return  $data[$tag];
			case 'newsPost':	
					$data = array(
					"title" => "{$params['name']} | blog ".Yii::app()->name,
					"desc" => "{$params['name']} read our news. ".Yii::app()->name,
					"h1" => "",				
				);
				return  $data[$tag];
			case 'servers_ru':	
				$data = array(
					"title" => "{$params['name']} | игровой портал ".Yii::app()->name,
					"desc" => "Предлагаем вашему внимаю бесплатный Counter-Strike Source {$params['name']}, хороший онлайн и адекватные админы . Присоиденяйтесь, {$params['address']} ☕ ",
					"h1" => "",					
				);
				return  $data[$tag];
			case 'newsPost_ru':	
					$data = array(
					"title" => "{$params['name']} | новости ".Yii::app()->name,
					"desc" => "{$params['name']} Читайте новости игрового портала ".Yii::app()->name.', на нашем сайте.',
					"h1" => "",				
				);
				return  $data[$tag];			
		}					
	}
    
	public static function getCurrentUriPath(){
		return Yii::app()->urlManager->parseUrl(Yii::app()->request).(!empty($_GET) ? '|'.serialize($_GET) : '');
	}
	
	public function getTitle(){
		if($this->title)
			return CHtml::encode($this->title);
		return CHtml::encode($this->metaData($this->getType(), 'title', $this->templateVariables));
	}

	public function setTitle($value = null){
		$this->title = $value;
	}

	public function getDescription(){
		if($this->description)
			return CHtml::encode($this->description);
		return CHtml::encode($this->metaData($this->getType(), 'desc', $this->templateVariables));				
	}

	public function setDescription($value = null){
		$this->description = $value;
	}
	
	public function getText(){
		return $this->text;
	}

	public function setText($value = null){
		$this->text = $value;
	}
	
	public function getH1(){
		if($this->h1)
			return CHtml::encode($this->h1);
		return CHtml::encode($this->metaData($this->getType(), 'h1', $this->templateVariables));	
	}

	public function setH1($value = null){
		$this->h1 = $value;
	}	
	
	public function setRobots($index, $follow){
		$this->index = $index;
		$this->follow = $follow;
	}
	
	
	public function getRobots(){
		$index = 'index';
		$follow = 'follow';
		if($this->index == 0){
			$index = 'noindex';
		}
		if($this->follow == 0){
			$follow = 'nofollow';
		}

		return "$index,$follow";
	}

}