<div class="pay_choose">
    <h1><?=Yii::t('vip', 'Select payment system')?>:</h1>
    <? foreach($payments as $name => $payment): ?>
        <div class="paymant">
            <div class="pay_system_box">
            	<a href="<?=$payment->url?>">
                	<img alt="<?=$name?>" title="<?=$name?>" src="<?=$payment->logo?>">
					<span><?=$name?></span>
                </a>
            </div>
        </div>
    <? endforeach; ?>
    <?if(isset($prevUlr) && $prevUlr):?>
        <div class="back"><a href="<?=$prevUlr?>"><?=Yii::t('vip', 'Back'); ?></a></div>
    <?endif;?>
</div>