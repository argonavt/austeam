<div class="pay_pages">
<div class="user_page">
	<div class="left_part">
    	<h1>			
			<?=$playerName?>
		</h1>		
        <img alt="<?=$playerName?>" src="<?=$avatarFull?>">
        <span class="skype">
			<? if($skype): ?>
				<a href="skype: <?=$skype?>?chat"><?=$skype?></a>
			<? endif; ?>				
        </span>
    </div>
    <div class="center_part">
			<span class="profile_range_icons">
				<? if(isset($admin) && $admin): ?>
					<span title="Admin">A</span>
				<? endif;?>
				<!--	<li title="Vip">V</li>-->
				<? if($is_steam): ?>
					<span title="Steam">S</span>
				<? endif;?>					
			</span>		
		 <table>
			<tr>
				<td><?=Yii::t('profile', 'Balance')?></td>
				<td><span><a href="#">0 AUS</a></span></td>
			</tr>
			<tr>
				<td>Steam ID</td>
				<? if($is_steam):?>
					<td><a href="http://steamcommunity.com/profiles/<?=$steamID64?>"><?=$steamID?></a></td>
				<?else:?>
					<td><?=$steamID?></td>
				<?endif;?>
			</tr>		
			<? if(isset($sinceFrom)): ?>
				<tr>
					<td><?=Yii::t('profile', 'Registered in')?></td>
					<td><?=$sinceFrom?></td>
				</tr>
			<? endif; ?>
			<? if($mostActiveServerID): ?>	
				<tr>
					<td><?=Yii::t('profile', 'Most active at')?></td>
					<td><a href="<?=Yii::app()->createUrl('monitor/frontend/deteil',array('server' => $mostActiveServerID))?>"><?=$mostActiveServerName?></a></td>
				</tr>
			<? endif; ?>
			<tr>
				<td><?=Yii::t('profile', 'Comments')?></td>
				<td><a href="#">0</a></td>
			</tr>
			<? if(isset($banCount) && $showBanCounts): ?>	
				<tr>
					<td><?=Yii::t('profile', 'Bans number')?></td>
					<td><a href="#"><?=$banCount?></a></td>
				</tr>
			<? endif; ?>		
		</table>
    </div>
</div>
</div>