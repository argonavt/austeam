<?php
class FrontendController extends FController
{

	public function actionIndex() {
		$params = array();
		$pageId = Yii::app()->request->getQuery('page');
		$page = new Pages();
		$page = $page->getPage($pageId);
		$params['data'] = $page;

		$this->render('index', $params);
	}

}
