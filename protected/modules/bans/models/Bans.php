<?php

/**
 * This is the model class for table "sb_bans".
 *
 * The followings are the available columns in table 'sb_bans':
 * @property integer $bid
 * @property string $ip
 * @property string $authid
 * @property string $name
 * @property integer $created
 * @property integer $ends
 * @property integer $length
 * @property string $reason
 * @property integer $aid
 * @property string $adminIp
 * @property integer $sid
 * @property string $country
 * @property integer $RemovedBy
 * @property string $RemoveType
 * @property integer $RemovedOn
 * @property integer $type
 * @property string $ureason
 */
class Bans extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sb_bans';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('reason', 'required'),
            array('created, ends, length, aid, sid, RemovedBy, RemovedOn, type', 'numerical', 'integerOnly'=>true),
            array('ip, adminIp', 'length', 'max'=>32),
            array('authid', 'length', 'max'=>64),
            array('name', 'length', 'max'=>128),
            array('country', 'length', 'max'=>4),
            array('RemoveType', 'length', 'max'=>3),
            array('ureason', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('bid, ip, authid, name, created, ends, length, reason, aid, adminIp, sid, country, RemovedBy, RemoveType, RemovedOn, type, ureason', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'bid' => 'Bid',
            'ip' => 'Ip',
            'authid' => 'Authid',
            'name' => 'Name',
            'created' => 'Created',
            'ends' => 'Ends',
            'length' => 'Length',
            'reason' => 'Reason',
            'aid' => 'Aid',
            'adminIp' => 'Admin Ip',
            'sid' => 'Sid',
            'country' => 'Country',
            'RemovedBy' => 'Removed By',
            'RemoveType' => 'Remove Type',
            'RemovedOn' => 'Removed On',
            'type' => 'Type',
            'ureason' => 'Ureason',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('bid',$this->bid);
        $criteria->compare('ip',$this->ip,true);
        $criteria->compare('authid',$this->authid,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('created',$this->created);
        $criteria->compare('ends',$this->ends);
        $criteria->compare('length',$this->length);
        $criteria->compare('reason',$this->reason,true);
        $criteria->compare('aid',$this->aid);
        $criteria->compare('adminIp',$this->adminIp,true);
        $criteria->compare('sid',$this->sid);
        $criteria->compare('country',$this->country,true);
        $criteria->compare('RemovedBy',$this->RemovedBy);
        $criteria->compare('RemoveType',$this->RemoveType,true);
        $criteria->compare('RemovedOn',$this->RemovedOn);
        $criteria->compare('type',$this->type);
        $criteria->compare('ureason',$this->ureason,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Bans the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}