<?php
/**
 * This is the model class for table "hlstats_Servers".
 *
 * The followings are the available columns in table 'hlstats_Servers':
 * @property string $serverId
 * @property string $address
 * @property string $port
 * @property string $name
 * @property string $game
 * @property string $publicaddress
 * @property string $statusurl
 * @property string $rcon_password
 */
class Servers extends CActiveRecord
{
	private static $showBar = true;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'hlstats_Servers';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('address', 'length', 'max'=>15),
            array('port', 'length', 'max'=>5),
            array('name, publicaddress', 'length', 'max'=>128),
            array('game', 'length', 'max'=>16),
            array('statusurl', 'length', 'max'=>255),
            array('rcon_password', 'length', 'max'=>64),
            array('addressAlias', 'length', 'max'=>32),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('serverId, address, port, name, game, publicaddress, statusurl, rcon_password, addressAlias', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'serverId' => 'Server',
            'address' => 'Address',
            'port' => 'Port',
            'name' => 'Name',
            'game' => 'Game',
            'publicaddress' => 'Publicaddress',
            'statusurl' => 'Statusurl',
            'rcon_password' => 'Rcon Password',
            'addressAlias' => 'Address Alias',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('serverId',$this->serverId,true);
        $criteria->compare('address',$this->address,true);
        $criteria->compare('port',$this->port,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('game',$this->game,true);
        $criteria->compare('publicaddress',$this->publicaddress,true);
        $criteria->compare('statusurl',$this->statusurl,true);
        $criteria->compare('rcon_password',$this->rcon_password,true);
        $criteria->compare('addressAlias',$this->addressAlias,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Server the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
	
	public static function getBarStatus(){
		return self::$showBar;
	}

	public static function hideBar(){
		self::$showBar = false;
	}	
	
}