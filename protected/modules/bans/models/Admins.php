<?php

/**
 * This is the model class for table "sb_admins".
 *
 * The followings are the available columns in table 'sb_admins':
 * @property integer $aid
 * @property string $user
 * @property string $authid
 * @property string $password
 * @property integer $gid
 * @property string $email
 * @property string $validate
 * @property integer $extraflags
 * @property integer $immunity
 * @property string $srv_group
 * @property string $srv_flags
 * @property string $srv_password
 * @property integer $lastvisit
 */
class Admins extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sb_admins';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user, password, gid, email, extraflags', 'required'),
            array('gid, extraflags, immunity, lastvisit', 'numerical', 'integerOnly'=>true),
            array('user, authid, srv_flags', 'length', 'max'=>64),
            array('password, email, validate, srv_group, srv_password', 'length', 'max'=>128),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('aid, user, authid, password, gid, email, validate, extraflags, immunity, srv_group, srv_flags, srv_password, lastvisit', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'aid' => 'Aid',
            'user' => 'User',
            'authid' => 'Authid',
            'password' => 'Password',
            'gid' => 'Gid',
            'email' => 'Email',
            'validate' => 'Validate',
            'extraflags' => 'Extraflags',
            'immunity' => 'Immunity',
            'srv_group' => 'Srv Group',
            'srv_flags' => 'Srv Flags',
            'srv_password' => 'Srv Password',
            'lastvisit' => 'Lastvisit',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('aid',$this->aid);
        $criteria->compare('user',$this->user,true);
        $criteria->compare('authid',$this->authid,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('gid',$this->gid);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('validate',$this->validate,true);
        $criteria->compare('extraflags',$this->extraflags);
        $criteria->compare('immunity',$this->immunity);
        $criteria->compare('srv_group',$this->srv_group,true);
        $criteria->compare('srv_flags',$this->srv_flags,true);
        $criteria->compare('srv_password',$this->srv_password,true);
        $criteria->compare('lastvisit',$this->lastvisit);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Admins the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}