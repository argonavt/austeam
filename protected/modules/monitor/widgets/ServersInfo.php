<?php

Yii::import('monitor.models.*');
class ServersInfo extends CWidget
{
	public function run() {
		if(!Servers::getBarStatus()){
			return;
		}	
		
		// Получаем список серверов
		$servers = Servers::model()->findAll(array(
			'select'=>'name, address, port, serverId',
			'order' => 'name'
		));

		$params['servers'] = array();
		$playerCounter = 0;
		foreach($servers as $key => $serverInfo){
		
			$server = new CServerInfo($serverInfo->address, $serverInfo->port);
			$info = $server->getInfo();
			
			$params['servers'][$serverInfo->serverId]['name'] = $serverInfo->name;
			if($info){			
				$params['servers'][$serverInfo->serverId]['status'] = true;
				$params['servers'][$serverInfo->serverId]['map'] = strlen($info['map']) > 10 ? substr($info['map'], 0, 10).'...' : $info['map'];
				$params['servers'][$serverInfo->serverId]['mapName'] = $info['map'];
				$params['servers'][$serverInfo->serverId]['maxPlayers'] = $info['maxplayers'];
				$params['servers'][$serverInfo->serverId]['numberOfPlayers'] = $info['numplayers'];
				$playerCounter += $info['numplayers'];
			}else{			
				$params['servers'][$serverInfo->serverId]['status'] = false;
			}
		}				
		
		$params['playerCounter'] = $playerCounter;
		
		$this->render('application.modules.monitor.views.frontend.widget-infoblock', $params);
	}	
}

