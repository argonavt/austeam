<div class="user_page">
	<div class="left_part">
        <img alt="<?=$playerName?>" src="<?=$avatarFull?>">
        <span class="skype">
			<? if($skype): ?>
				<a href="skype: <?=$skype?>?chat"><?=$skype?></a>
			<? endif; ?>				
        </span>
    </div>
    <div class="center_part">
   		<a class="user_activity" href="#"><?=$status?></a>
		<h1>			
			<?=$playerName?>
			<span class="profile_range_icons">
				<? if(isset($admin) && $admin): ?>
					<span title="Admin">A</span>
				<? endif;?>
				<!--	<li title="Vip">V</li>-->
				<? if($is_steam): ?>
					<span title="Steam">S</span>
				<? endif;?>					
			</span>		
		</h1>		

        <span class="user_status">
			<? if($status == 'online'):?>	
				<a href="<?=Yii::app()->createUrl('monitor/frontend/deteil',array('server' => $servId))?>"><?=$JoinServerName?></a>
				<a href="steam://connect/<?=$ip?>:<?=$port?>"><?=Yii::t('profile', 'Join in')?></a>			
			<? elseif($lastVisit):?>
				<?=Yii::t('profile', 'Last Visit in')?> <?=$lastVisit?>
			<? endif;?>
		</span>
		<table>
			<tr>
				<td><?=Yii::t('profile', 'Balance')?></td>
				<td><span><a href="#">0 AUS</a></span></td>
			</tr>
			<tr>
				<td>Steam ID</td>
				<? if($is_steam):?>
					<td><a href="http://steamcommunity.com/profiles/<?=$steamID64?>"><?=$steamID?></a></td>
				<?else:?>
					<td><?=$steamID?></td>
				<?endif;?>
			</tr>		
			<? if(isset($sinceFrom)): ?>
				<tr>
					<td><?=Yii::t('profile', 'Registered in')?></td>
					<td><?=$sinceFrom?></td>
				</tr>
			<? endif; ?>
			<? if($mostActiveServerID): ?>	
				<tr>
					<td><?=Yii::t('profile', 'Most active at')?></td>
					<td><a href="<?=Yii::app()->createUrl('monitor/frontend/deteil',array('server' => $mostActiveServerID))?>"><?=$mostActiveServerName?></a></td>
				</tr>
			<? endif; ?>
			<tr>
				<td><?=Yii::t('profile', 'Comments')?></td>
				<td><a href="#">0</a></td>
			</tr>
			<? if(isset($banCount) && $showBanCounts): ?>	
				<tr>
					<td><?=Yii::t('profile', 'Bans number')?></td>
					<td><a href="#"><?=$banCount?></a></td>
				</tr>
			<? endif; ?>		
		</table>
    </div>
	<? if($accessToEditing): ?>	
		<a class="steam_id" href="<?=Yii::app()->createUrl('/profile/'.UsersConnects::getSteamId64().'/edit')?>"><?=Yii::t('profile', 'Edit profile')?></a>
	<? endif;?>
</div>