<div class="pay_pages">
	<?=CHtml::beginForm('/vip_test/profile'); ?>
    <h1><?=Yii::t('vip', 'Enter your Steam ID')?>:</h1>
    <?=CHtml::activeTextField($model, 'steam_id', array('placeholder' => 'STEAM_0:0:00000000')); ?>
    <p><?=Yii::t('vip', '(You can check your Steam ID, type <em>!steamid</em> in the game)')?></p>
    <div class="next"><?=CHtml::submitButton(Yii::t('vip', 'Next')); ?></div>
    <?=CHtml::endForm(); ?>	
</div>
