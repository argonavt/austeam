<div id="lang-switcher">
	<ul class="language">				
		<? foreach($langs as $lang => $params) : ?>
		<li><a <?=Yii::app()->language == $lang ? 'class="active"' : ''?> target="_self" href="<?=$params['url']?>"><?=$lang?></a></li>		
		<? endforeach; ?>
	</ul>	
</div>

