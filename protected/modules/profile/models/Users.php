<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $id
 * @property string $noSteamID
 * @property string $skype
 * @property string $mail
 * @property string $AUS
 */
class Users extends CActiveRecord
{
	public $SteamID;
	public $noSteamID;
	public $skype;
	public $mail;
	public $AUS;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('steamID', 'required'),
			array('noSteamID', 'ChackSteamID'),
			array('skype', 'length', 'max'=>32),
			array('mail', 'chackEmail'),
			array('AUS', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, steamID, noSteamID, skype, mail, AUS', 'safe', 'on'=>'search'),
		);
	}
	
	public function chackSteamID($attribute,$params) {
		if(!$this->$attribute){
			return true;
		}	
		
		if(strpos(strtoupper($this->$attribute), 'STEAM_0:') !== 0){
			$this->addError('noSteamID', Yii::t('profile', 'Steam ID format is incorrect'));
		}

		$permitForSteamID = Yii::app()->db->createCommand()
			->select('count(*) rows')
			->from('hlstats_PlayerUniqueIds uniq')	
			->leftJoin('hlstats_Events_Connects gconn', 'uniq.playerId = gconn.playerId')	
			->leftJoin('users_connects sconn', 'sconn.ip  = gconn.ipAddress AND sconn.steam_id = "'.UsersConnects::getSteamId32(Yii::app()->request->getParam('userid')).'"')	
			->where('uniqueId = :steamID AND sconn.ip IS NOT NULL', array(':steamID'=> strtoupper($this->$attribute)))
			->queryRow();	


        if(!$permitForSteamID){
            $this->addError('noSteamID', Yii::t('profile', 'Steam id doesn\'t exist'));		
		}elseif($permitForSteamID['rows'] == 0){
			$this->addError('noSteamID', Yii::t('profile', 'You don\'t have permission for this Steam ID'));
		}
	}
	
	public function chackEmail($attribute,$params) {
		if(!$this->$attribute){
			return true;
		}		
		
		if(!preg_match('/.+@.+\..+/i', $this->$attribute)){
			$this->addError('mail', Yii::t('profile', 'Email is incorrect'));
		}			
		
		$permitEmail = Yii::app()->db->createCommand()
			->select('count(*) rows')
			->from('users')	
			->where('steamID != :steamID AND mail = :mail', array(':steamID'=> UsersConnects::getSteamId32(Yii::app()->request->getParam('userid')), ':mail' => $this->$attribute))
			->queryRow();			

        if($permitEmail && $permitEmail['rows'] == 1){
            $this->addError('mail', Yii::t('profile', 'Email has already used'));
		}
	}
	
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'steamID' => 'SteamID',
			'noSteamID' => 'No SteamID',
			'skype' => 'Skype',
			'mail' => 'Email',
			'AUS' => 'AUS',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('steamID',$this->steamID,true);
		$criteria->compare('noSteamID',$this->noSteamID,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('mail',$this->mail,true);
		$criteria->compare('AUS',$this->AUS,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getPrivateUserData($steamID32){		
		if(Yii::app()->cache->get('privateUserData_'.$steamID32)){
			$data = Yii::app()->cache->get('privateUserData_'.$steamID32);
		}else{
			$data = Yii::app()->db->createCommand()
					->from('users')
					->where('steamID = :steamId OR noSteamID = :noSteamID', array(':steamId'=> $steamID32, ':noSteamID' => $steamID32))
					->limit(1)				
					->queryRow();
			Yii::app()->cache->set('privateUserData_'.$steamID32, $data, 345600);
		}
		
		return $data;		
	}
	
	public function  getUserGameData($steamID32){	
		
		if(Yii::app()->cache->get('UserGameData_'.$steamID32)){
			$data = Yii::app()->cache->get('UserGameData_'.$steamID32);
		}else{
			$subquery = Yii::app()->db->createCommand()
				->select('authid, count(*) AS banCount')
				->from('sb_bans')
				->where('length >= 604800')
				->group('authid')
				->text;		
			$data = Yii::app()->db->createCommand()
					->select('names.lastName playerName, MAX(conn.eventTime) lastConnect,  adm.user admin, bans.banCount, serv.name serverName, conn.map, count(conn.serverId) count, serv.serverId')
					->from('hlstats_Events_Connects conn')	
					->leftJoin('hlstats_PlayerUniqueIds uniq', 'conn.playerId = uniq.playerId')
					->leftJoin('hlstats_Servers serv', 'serv.serverId = conn.serverId')
					->leftJoin('hlstats_Players names', 'names.playerId = uniq.playerId')
					->leftJoin('sb_admins adm', 'uniq.uniqueId = adm.authid')	
					->leftJoin('('.$subquery.') bans', 'bans.authid = uniq.uniqueId')	
					->where('uniq.uniqueId = :uniqueId AND conn.map <> ""', array(':uniqueId'=> $steamID32))
					->order('count DESC')
					->queryRow();

            
			Yii::app()->cache->set('UserGameData_'.$steamID32, $data, 3600);
		}	

		return $data;
	}
	
	public function getUserOnlineData($steamID32){
		if(Yii::app()->cache->get('UserOnlineData_'.$steamID32) !== false){
			$data = Yii::app()->cache->get('UserOnlineData_'.$steamID32);
		}else{
			$data = Yii::app()->db->createCommand()
					->select('serv.name, serv.port, serv.serverId')
					->from('hlstats_Events_PlayerAttackedPlayer atack')
					->leftJoin('hlstats_PlayerUniqueIds uniq', 'atack.playerId = uniq.playerId')	
					->leftJoin('hlstats_Servers serv', 'serv.serverId = atack.serverId')		
					->where('uniq.uniqueId = :uniqueId AND atack.eventTime >= date_sub(now(), interval 3 MINUTE)', array(':uniqueId'=> $steamID32))
					->order('atack.eventTime DESC')
					->limit(1)				
					->queryRow();
			if(!$data){
				$data = array();
			}
			Yii::app()->cache->set('UserOnlineData_'.$steamID32, $data, 180);
		}		
		return $data;
	}
	
	public function getSteamData($steamID64){
		if(Yii::app()->cache->get('userSteam_'.$steamID64)){
			$steam = Yii::app()->cache->get('userSteam_'.$steamID64);
		}else{		
			$output = Yii::app()->curl->setOption(CURLOPT_TIMEOUT, 5)->get("http://steamcommunity.com/profiles/".$steamID64."/?xml=1");		
			$steamXML = @simplexml_load_string($output);	
			$steam = array();		
			if(is_object($steamXML)){
				
				if($steamXML->avatarFull){
					$steam['avatarFull'] = (string)$steamXML->avatarFull;
				}
				if((string)$steamXML->steamID){
					$steam['playerName'] = (string)$steamXML->steamID;
				}				
				Yii::app()->cache->set('userSteam_'.$steamID64, $steam, 86400);					
			}
		}
		return $steam;
	}
	
	
}
