<?php
class FrontendController extends FController
{
	public function actionIndex() {
		$params = array();
		
		if(Yii::app()->request->isAjaxRequest){
			$params = array();
			$criteria= new CDbCriteria(); 
			$criteria->select = array('t.steam', 't.name', 't.kills', 't.deaths', 't.knife', 'hlstats_Players.lastName AS lastName');
			$criteria->join = 'LEFT JOIN hlstats_PlayerUniqueIds ON hlstats_PlayerUniqueIds.uniqueId = t.steam '
							. 'LEFT JOIN hlstats_Players ON hlstats_Players.playerId = hlstats_PlayerUniqueIds.playerId ';
			$criteria->condition = 't.lastconnect > :month';
			$criteria->params = array(':month'=> (time() - 60 * 60 * 24 * 30));
			$criteria->order = 'score DESC';
			$criteria->limit = '100';
			$params['topPlayers'] = Rank::model()->findAll($criteria);				
			$this->renderPartial('_ajaxStatsList',$params);
			Yii::app()->end();
		}

		$this->render('index', $params);
	}
}
