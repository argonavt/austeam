<?php

class FrontendController extends FController
{	
	public function actionIndex(){		
		$steamID = UsersConnects::getSteamIDfromIP();
		if($steamID && is_string($steamID)){
			$this->redirect(Yii::app()->createUrl(Yii::app()->controller->module->id.'/profile?steamid='.$steamID));			
		}		

		$data = array();
		$data['model'] = new PaymentLog;

		$session = Yii::app()->session->get('vip');
		if(isset($session['steamID'])){
			$data['model']->steam_id = UsersConnects::getSteamId32($session['steamID']);
		}
		$this->render('index', $data);
	}
	
	public function actionProfile(){		
		if(Yii::app()->request->getQuery('steamid')){
			$steamID = Yii::app()->request->getQuery('steamid');
		}elseif(Yii::app()->request->getPost('PaymentLog')){
			$post = Yii::app()->request->getPost('PaymentLog');
			$steamID = $post['steam_id'];
		}else{
			$this->redirect(Yii::app()->createUrl(Yii::app()->controller->module->id));
		}
				
		Yii::app()->session->add('vip', array(
			'steamID' => $steamID			
		));		
		$data['steamID'] = UsersConnects::getSteamId64($steamID);
		$data['nextUlr'] = Yii::app()->createUrl(Yii::app()->controller->module->id.'/payment/');
		$data['prevUlr'] = Yii::app()->createUrl(Yii::app()->controller->module->id);
		
		$this->render('profile', $data);
	}		

	public function actionPayment(){
		$data = array();
	
		$payments = Yii::app()->db->createCommand()
			->select('name, settings')
			->from('paysystems')	
			->where('active= :active ', array(':active'=> 1))
			->queryAll();	
		
		$data['payments'] = array();
		foreach($payments as $payment){
			if(!$payment['settings']){
				continue;
			}
			$settings = json_decode($payment['settings']);
			$data['payments'][$payment['name']] = $settings;	
			
			$data['payments'][$payment['name']]->url = Yii::app()->createUrl(Yii::app()->controller->module->id.'/payment/'.$settings->alias);
		}	

		$data['prevUlr'] = Yii::app()->createUrl(Yii::app()->controller->module->id.'/profile/');		
		
		$this->render('payment', $data);
	}	
	
	public function actionShowPayment(){
		$paymentName = Yii::app()->request->getQuery('payment');
		$paymentAction = Yii::app()->request->getQuery('action');

		if(class_exists($paymentName)){
			$payment = new $paymentName($this);
			$payment->$paymentAction();
		}		
	}
}
