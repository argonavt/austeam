<table class="table table-deteil">	
    <tr>
        <td><?=Yii::t('server', 'Name')?></td>
        <td><?=Yii::t('server', 'Score')?></td>
        <td><?=Yii::t('server', 'Time')?></td>
    </tr>					
    <? foreach($server['players'] as $player): ?>		
        <tr>
			<?if(isset($player['uniqueId'])):?>
				<td>
					<a href="<?=Yii::app()->createUrl('/profile/'.$player['uniqueId'])?>"><?=$player['name']?></a>
					<div class="range_icons">	
						<? if($player['admin']): ?>
							<span title="Admin">A</span>
						<? endif;?>
						<? if(isset($player['banCount']) && $server['showBanCounts']): ?>
							<span title="<?=Yii::t('profile', 'Bans number')?>"><?=$player['banCount']?></span>
						<? endif;?>							
						<!--	 <span title="Vip">V</span>-->
						<!--	 <span title="Steam">S</span>-->					
			    	</div>
				</td>
			<? else:?>
				<td><a href="/search?serach-word=<?=htmlspecialchars($player['name'])?>"><?=$player['name']?></a></td>
			<? endif;?>  
            <td><?=$player['score']?></td>
            <td><?=$player['connectTime']?></td>
        </tr>	
    <? endforeach; ?>
</table>

