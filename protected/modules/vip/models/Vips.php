<?php
/**
 * Description of Users
 *
 * @author argonavt
 */
class Vips {
	private $file;
	private $users = array();
	
	public function __construct() {
		$this->file  = Yii::getPathOfAlias('webroot').'/files/vip/users.ini';
		$file = file_get_contents($this->file);
		$this->parseStr($file);

	}
	
	private function parseUser($data){
		$key = $data[1];
		$vipData = explode("\n", trim($data[2]));
		foreach($vipData as $row){
			$row = trim($row);
			$find = preg_match('#^"([A-z_]+)"\s+"(.+)"$#', $row, $match);
			$this->users[$key][$match[1]] = $match[2];
		}
	}
	
	
	private function parseStr($str){
		preg_replace_callback('#"(STEAM_[:0-9]+)"\s*{(.+?)}\s#is',  array($this, 'parseUser'), $str);
	}
	
	public function saveFile(){
		$str = "\"VIP_USERS\"\n";
		$str .= "{\n";
		
		foreach($this->users as $steamID => $data){
			$str .= "\t\"$steamID\"\n";
			$str .= "\t{\n";		
			foreach($data as $key => $value){
				$str .= "\t\t\"$key\"\t\t\"$value\"\n";
			}	
			$str .= "\t}\n\n";
		}
		$str .= "}";
		
		file_put_contents($this->file, $str);
	}	
	
	public function getInfo($steamID){
		if(isset($this->users[$steamID])){
			return $this->users[$steamID];
		}
		return false;
	}
	
	
	
	public function addUser($steamID, $name, $duration = 10080, $type = ''){
		$user = array();
		$user['name'] = $name;
		$user['expires'] = $duration * 60;
		switch($type){
			default : $user['vip_group'] = 'Classic';
		}
		
		if(isset($this->users[$steamID]) && $this->users[$steamID]['expires'] > time()){
			$user['expires'] += $this->users[$steamID]['expires'];
		}else{
			$user['expires'] = time() + $user['expires'];
		}
		
		$this->users[$steamID] = $user;
		
	}
	
}
