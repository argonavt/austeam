<?php
return array(
    'Last Visit in' => 'Последний визит',
	'Balance' => 'Баланс',
	'Registered in' => 'На сервере Austeam с',
	'Comments' => 'Комментариев',
	'Most active at' => 'Наибольшая активность на',
	'Edit profile' => 'Редактировать профиль',
	'Join in' => 'Присоидениться',
	'Bans number' => 'Количество банов',
	'Return back' => 'Вернуться назад',
	'Steam id doesn\'t exist' => 'Данный Steam ID не существует',
	'Save' => 'Сохранить',
	'Change an avatar' => 'Изменить аватар',
	'Changes have been saved' => 'Изменения сохранены',
	'You don\'t have permission for this Steam ID' => 'У вас нету доступа к этому Steam ID',
	'Steam ID format is incorrect' => 'Указан некорретный формат Steam ID',
	'Email has already used' => 'Данный email уже используется другим пользователем',
	'Email is incorrect' => 'Емейл адрес некорректен',
);

