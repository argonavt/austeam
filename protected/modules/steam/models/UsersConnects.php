<?php

/**
 * This is the model class for table "users_connects".
 *
 * The followings are the available columns in table 'users_connects':
 * @property string $id
 * @property string $steam_id
 * @property string $eventTime
 * @property string $ip
 * @property string $user_hash
 * @property string $openid_sig
 * @property string $countrycode
 */
class UsersConnects extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_connects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('steam_id, eventTime, ip, user_hash, openid_sig, countrycode', 'required'),
			array('steam_id', 'length', 'max'=>30),
			array('ip', 'length', 'max'=>15),
			array('user_hash', 'length', 'max'=>32),
			array('openid_sig', 'length', 'max'=>28),
			array('countrycode', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, steam_id, eventTime, ip, user_hash, openid_sig, countrycode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'steam_id' => 'Steam',
			'eventTime' => 'Event Time',
			'ip' => 'Ip',
			'user_hash' => 'User Hash',
			'openid_sig' => 'Openid Sig',
			'countrycode' => 'Countrycode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('steam_id',$this->steam_id,true);
		$criteria->compare('eventTime',$this->eventTime,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('user_hash',$this->user_hash,true);
		$criteria->compare('openid_sig',$this->openid_sig,true);
		$criteria->compare('countrycode',$this->countrycode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersConnects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function checkUser($openid_sig){
		return (bool) Yii::app()->db->createCommand()
			->from('users_connects')
			->where('openid_sig=:openid_sig AND eventTime >= date_sub(now(), interval 30 day)', array(':openid_sig' => $openid_sig))
			->queryRow();

	}
	
	public static function getSteamId32($steamid64){
		$id = array('STEAM_0');
		$id[1] = substr($steamid64, -1, 1) % 2 == 0 ? 0 : 1;
		$id[2] = bcsub($steamid64, '76561197960265728');
		$id[2] = bcsub($id[2], $id[1]);
		list($id[2], ) = explode('.', bcdiv($id[2], 2), 2);
		return  implode(':', $id);			
	}
	
	
	public static function getSteamId64($steamid32 = 'self'){
		if($steamid32 == 'self'){	
			
			if(!Yii::app()->request->cookies['openid_sig']){
				return false;
			}
			
			$steamid32 = Yii::app()->db->createCommand()
				->from('users_connects')
				->where('openid_sig=:openid_sig AND eventTime >= date_sub(now(), interval 30 day)', array(':openid_sig' => Yii::app()->request->cookies['openid_sig']->value))
				->queryRow();
			$steamid32 = $steamid32['steam_id'];
		}
		if(!$steamid32){
			return false;
		}

		list( , $m1, $m2) = explode(':', $steamid32, 3);
		list($steam_cid, ) = explode('.', bcadd((((int) $m2 * 2) + $m1), '76561197960265728'), 2);	
		
		return $steam_cid;

	}	

	public static function checkAdminRights(){
	
		if(!Yii::app()->request->cookies['openid_sig']){
			return false;
		}
		$steamId = self::getSteamId32(self::getSteamId64());
		
		$users = array('STEAM_0:1:126745841');
		
		if(in_array($steamId, $users)){
			return true;
		}
		
		
		return (bool) Yii::app()->db->createCommand()
			->from('sb_admins')
			->where('authid=:authid', array(':authid' => $steamId))
			->queryRow();
	}

	public static function getSteamIDfromIP($ip = false){
		if(!$ip){
			$ip = Yii::app()->request->userHostAddress;
		}
		
		$data = Yii::app()->db->createCommand()
			->select('uniqueId, count(`uniqueId`) count')
			->from('hlstats_Events_Connects')
			->leftJoin('hlstats_PlayerUniqueIds', 'hlstats_PlayerUniqueIds.playerId = hlstats_Events_Connects.playerId')
			->where('ipAddress=:ipAddress', array(':ipAddress' => $ip))
			->group('uniqueId')
			->order('count DESC')
			->queryAll();

		if($data){
			return $data[0]['uniqueId'];
		}
		return false;
	}	
	
}
