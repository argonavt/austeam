
<? foreach($posts as $post) :?>
	<div class="post">
		<a href="<?=Yii::app()->createUrl('blog/frontend/deteil',array('post' => $post['id']))?>"><img class="post-preview" src="/images/blog/small/<?=$post['img']?>" title="<?=$post['name']?>" alt="<?=Yii::t('blog', 'foto')?> - <?=$post['name']?>"></a>
			<div class="post-date"><!--views - <?=$post['reviews']?> <?=$post['date']?>-->
            	<ul>
                	<li><?=$post['reviews']?></li>
                    <li><?=$post['date']?></li>
                    <li><?=$post['time']?></li>
                </ul>
            </div>
			<h2><?=$post['name']?></h2>
			<div class="annotation"><?=$post['annotation']?></div>
			<div class="read-more"><a href="<?=Yii::app()->createUrl('blog/frontend/deteil',array('post' => $post['id']))?>"><?=Yii::t('blog', 'more')?></a></div>
			<div class="clear"></div>
		</div>
<? endforeach; ?>





