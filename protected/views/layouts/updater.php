<!DOCTYPE html>
<head>
    <title>Updater</title>
    <meta name="robots" content="<?=Yii::app()->seo->getRobots(); ?>">
    <?Yii::app()->clientScript->registerCSSFile('/css/updater.css');?>
    <?Yii::app()->clientScript->registerCSSFile('/css/ui/jquery-ui.css');?>
    <?Yii::app()->clientScript->registerCSSFile('/css/ui/jquery-ui.structure.css');?>
    <?Yii::app()->clientScript->registerCSSFile('/css/ui/jquery-ui.theme.css');?>
	<script type="text/javascript">var initFunctions = [];</script>
</head>
<body>
    <? echo $content; ?>
	<script type="text/javascript" src="/js/updater/jquery-ui.min.js"></script>
    <?Yii::app()->clientScript->registerScriptFile('/js/updater/jquery-1.11.3.min.js', CClientScript::POS_END);?>
    <?Yii::app()->clientScript->registerScriptFile('/js/updater/jquery-ui.min.js', CClientScript::POS_END);?>
    <?Yii::app()->clientScript->registerScriptFile('/js/updater/scripts.js', CClientScript::POS_END);?>
</body>
</html>