<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?=Yii::app()->seo->title?></title>
	<meta name="description" content="<?= CHtml::encode(Yii::app()->seo->description); ?>" />
	<meta name="robots" content="<?=Yii::app()->seo->getRobots(); ?>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name=viewport content="width=device-width, initial-scale=1">
	<?Yii::app()->clientScript->registerCSSFile('/css/default.css');?>
	<?Yii::app()->clientScript->registerCSSFile('/css/content.css');?>
	<?Yii::app()->clientScript->registerCSSFile('/css/nanoscroller.css');?>
    <?$this->widget('application.widgets.Hreflang')?>
	<script type="text/javascript">var initFunctions = [];</script>
</head>
<body>
	<header>
		<div  class="center">
			<div class="right-main"> 
                <div class="right-box">
                        <span>Counter Strike Source</span> 
                        <nav>
							<? switch(Yii::app()->language):
								case 'ru':
									$this->widget('zii.widgets.CMenu',array(
										'items'=>array(
											array('label'=> Yii::t('menu', Yii::t('menu', 'Home')), 'url'=>array('/'.Yii::app()->defaultController)),
//											array('label'=> Yii::t('menu', Yii::t('menu', 'VIP')), 'url'=> '#',
//											'items'=>array(
//												array('label'=> Yii::t('menu', Yii::t('menu', 'Приемущества VIP')), 'url'=>array('/page/frontend', 'page' => '6')),
//												array('label'=> Yii::t('menu', Yii::t('menu', 'Приобрести VIP')), 'url'=>array('/vip-guest/')),
//											)),											
											array('label'=> Yii::t('menu', Yii::t('menu', 'Download')), 'url'=> '#',
												'items'=>array(
													array('label'=> Yii::t('menu', Yii::t('menu', 'Counter-Strike:Source')), 'url'=>array('/page/frontend', 'page' => '2')),
//													array('label'=> Yii::t('menu', Yii::t('menu', 'Patch 86 -> 87')), 'url'=>array('/files/patch.rar')),
												)												
											),
											array('label'=> Yii::t('menu', Yii::t('menu', 'Bans')), 'url'=>array('/bans/frontend')),
											array('label'=> Yii::t('menu', Yii::t('menu', 'Monitor')), 'url'=>array('/monitor/frontend')),
											array('label'=> Yii::t('menu', 'Rules'), 'url'=>array('/page/frontend', 'page' => '5')),
											array('label'=> Yii::t('menu', Yii::t('menu', 'Top')), 'url'=>array('/stats/frontend')),
										),
									));
									break;
								default:
									$this->widget('zii.widgets.CMenu',array(
										'items'=>array(
											array('label'=> Yii::t('menu', 'Home'), 'url'=>array('/'.Yii::app()->defaultController)),
//											array('label'=> Yii::t('menu', Yii::t('menu', 'VIP')), 'url'=> '#',
//											'items'=>array(
//												array('label'=> Yii::t('menu', Yii::t('menu', 'Advantages of VIP')), 'url'=>array('/page/frontend', 'page' => '7')),
//												array('label'=> Yii::t('menu', Yii::t('menu', 'To buy a VIP account')), 'url'=>array('/vip-guest/')),
//											)),											
											array('label'=> Yii::t('menu', Yii::t('menu', 'Download')), 'url'=> '#',
												'items'=>array(
													array('label'=> Yii::t('menu', Yii::t('menu', 'Counter-Strike:Source')), 'url'=>array('/page/frontend', 'page' => '1')),
//													array('label'=> Yii::t('menu', Yii::t('menu', 'Patch 86 -> 87')), 'url'=>array('/files/patch.rar')),
												),												
											),
											array('label'=> Yii::t('menu', 'Bans'), 'url'=>array('/bans/frontend')),
											array('label'=> Yii::t('menu', 'Monitor'), 'url'=>array('/monitor/frontend')),	
											array('label'=> Yii::t('menu', 'Rules'), 'url'=>array('/page/frontend', 'page' => '8')),										
											array('label'=> Yii::t('menu', 'Top'), 'url'=>array('/stats/frontend'))
										),
									)); 
							 endswitch; ?>
                        </nav>      
                </div>    
            	<div class="left-box">
					<?$this->widget('application.widgets.Steamauth')?>					
                </div>
                <div class="under_box">   
                	<div class="right-box">
                        <div class="socio-icon">
							<a rel="me nofollow" href="http://vk.com/austeamnet"><img title="vk" src="/images/frontend/vk-ico.png" alt=""></a>
                            <a href="skype:magicmonkeys?chat"><img title="skype: magicmonkeys" src="/images/frontend/skype-ico.png" alt=""></a>
                            <a href="mailto:magicmonkeys@yandex.ru"><img title="magicmonkeys@yandex.ru" src="/images/frontend/mail-ico.png" alt=""></a>
                        </div>  
                        <?$this->widget('application.widgets.LangSwitcher')?>
                       <form action="<?=Yii::app()->createUrl('search/frontend')?>" id="search">	
						   <input type="text" name="serach-word" placeholder="<?=Yii::t('menu', 'Name or Steam ID')?>">
                           <a onclick="document.getElementById('search').submit(); return false;" href="#"></a>
                       </form>
                    </div>
                </div> 
                <div class="logotype">
                    <div class="logo-text">
                        <a href="/">
                            <span>aus</span>
                            <span>team</span>
                        </a>
                    </div>
                    <a href="/">
                        <img src="<? echo Yii::app()->request->baseUrl; ?>/images/frontend/logo.png" title="Austeam" alt="Austeam">
                    </a>	
				</div>     
			</div>
		</div>
	</header>
    <div class="center">
    	<aside id="aside-left">
    		<!--<div>
				<ul>
					<li><a href="#">главная</a></li>
					<li><a href="#">online</a></li>
					<li><a href="#">статистика</a></li>
					<li><a href="#">скачать</a></li>            
					<li><a href="#">форум</a></li>            
				</ul>
			</div>-->
        </aside>
        <article>
			<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
					'activeLinkTemplate'=>'<span itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{url}"><span itemprop="title">{label}</span></a></span>',
					'homeLink'=>'<span itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/"><span itemprop="title">'.Yii::t('breadcrumbs', 'Home').'</span></a></span>',

				)); ?><!-- breadcrumbs -->
			<?php endif?>			
			<h1><?=Yii::app()->seo->h1?></h1>
        	<? echo $content; ?>
			<div id="text" class="nano">
				<div class="overthrow nano-content"> <?=Yii::app()->seo->text?></div>
			</div>
			<div id="comments">
				<?//$this->widget('application.modules.comments.widgets.PageComments')?>
			</div>
        </article>

		<?$this->widget('application.modules.monitor.widgets.ServersInfo')?>
		<?$this->widget('application.modules.html-blocks.widgets.Blocks')?>
    </div>
    <footer>
        <div class="center">
            <p id="copyright">
                © 2014 - <?=date('Y')?>
            </p>
        	<div id="footer">
                <div class="logotype-f">
                    <a href="/">
                        <span>aus</span>
                        <span>team</span>
                        <img src="<? echo Yii::app()->request->baseUrl; ?>/images/frontend/logo-f.png" title="Austeam" alt="Austeam">
                    </a>
                </div>	
            </div>
    		<!--<ul>
        		<li><a href="#">главная</a></li>
        		<li><a href="#">online</a></li>
        		<li><a href="#">статистика</a></li>
        		<li><a href="#">скачать</a></li>            
        		<li><a href="#">форум</a></li>            
            </ul>-->
        </div>  	
    </footer> 
	<!--<?=Yii::app()->seo->getCurrentUriPath()?>-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<?Yii::app()->clientScript->registerScriptFile('/js/hidden/metrika.js', CClientScript::POS_END);?>
	<?Yii::app()->clientScript->registerScriptFile('/js/jquery.nanoscroller.min.js', CClientScript::POS_END);?>
	<?Yii::app()->clientScript->registerScriptFile('/js/foot-scripts.js', CClientScript::POS_END);?>		
</body>
</html>
