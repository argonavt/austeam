<?php

class Vip_testModule extends CWebModule
{
	public function init()
	{	
		if (!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1'))) {
			header('Location: /', true, 301);
			die('393');
		}		
		
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'vip_test.payments.*',
			'vip_test.models.*',
			'vip_test.components.*',
			'vip_test.models.*',
			'steam.models.*',
			'profile.models.*',
			'monitor.models.*',
			'application.extensions.*'
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
