<?php

class FrontendController extends FController
{
	public function actionIndex()
	{
		$data = array();
		
		if(Yii::app()->request->getParam('serach-word')){
			$serachWord = trim(htmlspecialchars_decode(Yii::app()->request->getParam('serach-word')));
			if(strpos(strtoupper($serachWord), 'STEAM_0:') === 0){	
				$this->redirect(Yii::app()->createUrl('/profile/'.UsersConnects::getSteamId64($serachWord)));		
			}else{
				$data['players'] = Yii::app()->db->createCommand()
					->select('names.name, uniq.uniqueId')
					->from('hlstats_PlayerUniqueIds uniq')	
					->leftJoin('hlstats_PlayerNames names', 'names.playerId = uniq.playerId')	
					->where('names.name = :name ', array(':name'=> $serachWord))
					->order('names.numuses DESC')
					->queryAll();	
				if(count($data['players']) == 1){
					$this->redirect(Yii::app()->createUrl('/profile/'.UsersConnects::getSteamId64($data['players'][0]['uniqueId'])));
				}
			}
		}
		Yii::app()->seo->setTitle($serachWord);
		
		$this->render('index', $data);
	}
}