<h1><?=Yii::t('search', 'Result')?></h1>

<? if(isset($players) && $players):?>
	<table class="table table-monitor">
		<? foreach ($players as $player): ?>
			<tr><td><a href="<?=Yii::app()->createUrl('/profile/'.UsersConnects::getSteamId64($player['uniqueId']))?>"><?=$player['name']?></a></td><td><?=$player['uniqueId']?></td></tr>
		<? endforeach; ?>
	</table>
<?else:?>

<p><?=Yii::t('search', 'Nothing was found !')?></p>
<?endif;?>