<div class="sms_page">
	<h1><?=Yii::t('vip', 'SMS payment')?></h1>
	<? if(isset($smsForm)) :?>
        <div id="sms-form"><?= $smsForm ?></div>
    <?endif;?>
    <?if(isset($prevUlr) && $prevUlr):?>
        <div class="back"><a href="<?=$prevUlr?>"><?=Yii::t('vip', 'Back'); ?></a></div>
    <?endif;?>
	<p><?=$error?></p>
</div>