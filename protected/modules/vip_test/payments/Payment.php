<?php
abstract class Payment 
{
	 abstract  function index();
	 
	 protected function addVip($steamID){
		$user = new Users;
		$userData = $user->getUserGameData($steamID);
		$name = $userData['playerName'];		 
		 
		// добавляем вип
		$vips = new Vips;					
		$vips->addUser($steamID, $name);		
		$vips->saveFile();																

		// сохранияем в базу
		$log = new PaymentLog;
		$log->steam_id = $steamID;
		$log->paysystem = 1;
		$log->service_type = 1;
		$log->date = date('Y-m-d G:i:s');
		$log->ip = Yii::app()->request->userHostAddress;
		$log->save();		 
	 }
}
