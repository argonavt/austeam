<?php
 class Singleton {
	private static $classes = array();
    private function __construct(){ /* ... @return Singleton */ }
    private function __clone()    { /* ... @return Singleton */ }
    private function __wakeup()   { /* ... @return Singleton */ }
    public static function getInstance($name, $params = array()) {
        if ( !isset(self::$classes[$name]) ) {
			$reflect  = new ReflectionClass($name);
            self::$classes[$name] = $reflect->newInstanceArgs($params);
        }
        return self::$classes[$name];
    }
 }
