<aside class="aside-right">
	<div id="servers-info">
		<h4><?=Yii::t('monitor', 'Server list')?></h4>
		<table>
			<? foreach($servers as $id => $server): ?>
				<tr>
					<td><a href="<?=Yii::app()->createUrl('monitor/frontend/deteil',array('server' => $id))?>"><?=$server['name']?></a></td>
					<? if($server['status']): ?>
						<td title="<?=$server['mapName']?>"><?=$server['map']?></td>
						<td><?=$server['numberOfPlayers']?>/<?=$server['maxPlayers']?></td>
					<? else : ?>
						<td class="server-offline" colspan="2">offline</td>
					<? endif; ?>
				</tr>
			<? endforeach; ?>
			<tr> 
				<td><?=Yii::t('monitor', 'Total')?></td>
				<td></td>
				<td><?=$playerCounter?></td>
			</tr>
		</table>
	</div>
</aside>
