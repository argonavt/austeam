<?php

class Steamauth extends CWidget
{
	private function setImport($aliases)
	{
		foreach($aliases as $alias)
			Yii::import($alias);
	}	
	
	public function run() {
		
		$this->setImport(array(
			'steam.models.*',
		));
		
		$params = array();
		
		if(isset(Yii::app()->request->cookies['openid_sig']->value) && UsersConnects::checkUser(Yii::app()->request->cookies['openid_sig']->value)){
			$params['logged'] = true;
		}else{
			$params['logged'] = false;
		}


		
		$this->render('application.views.site.steamauth', $params);
	}	
}