<table class="table table-top">
	<tr>
    	<td>№</td>
		<td><?=Yii::t('ban', Yii::t('stats', 'Name'))?></td>
		<td><?=Yii::t('ban', Yii::t('stats', 'Kills'))?></td>
		<td><?=Yii::t('ban', Yii::t('stats', 'Deaths'))?></td>
		<td><?=Yii::t('ban', Yii::t('stats', 'Knife'))?></td>
	</tr>	
	<? foreach($topPlayers as $num => $player): ?>
		<tr>
        	<td><?=++$num?></td>
			<td><a href="<?=Yii::app()->createUrl('/profile/'.UsersConnects::getSteamId64($player->steam))?>"><?=($player->lastName ? $player->lastName : $player->name)?></a></td>
			<td><?=$player->kills?></td>
			<td><?=$player->deaths?></td>
			<td><?=$player->knife?></td>
		</tr>

	<? endforeach; ?>
</table>