<?php
//require_once('Steam/steam-condenser.php');
class GuestController extends FController
{
	public function actionIndex(){	
		$this->getSteamID();	
	}	
	
	public function actionStep(){	
		Yii::app()->seo->setTitle(Yii::t('vip', 'Buying VIP account'));		
		
		switch(Yii::app()->request->getQuery('step')){
			case '1': $this->getSteamID(); break;
			case '2': $this->checkProfile(); break;
			case '3': $this->selectPayment(); break;
			case 'sms': $this->paySms(); break;
			default : $this->actionIndex();
		}
	}		
	
	private function showSuccess($data){
		
		// Получаем список серверов
//		$servers = Servers::model()->findAll(array(
//			'select'=>'name, address, port, serverId, addressAlias',
//			'order' => 'name'
//		));
//
//		foreach($servers as $key => $serverInfo){
//			$server = new SourceServer($serverInfo->address, $serverInfo->port);
//			$server->rconAuth(Yii::app()->params['rcon']);
//			$server->rconExec('sm_vip_reload');
//		}	
		
		Yii::app()->seo->setH1(Yii::t('vip', 'VIP account was successfully added'));	
		
		$this->render('success', $data);
	}
	
	private function getSteamID(){	
		if(Yii::app()->request->isPostRequest){
			$formData = Yii::app()->request->getParam('PaymentLog');
			$this->redirect(Yii::app()->createUrl('vip-guest/step/2/?steamid='.UsersConnects::getSteamId64($formData['steam_id'])));
		}
		
		if(!Yii::app()->request->getQuery('step')){
			$steamID = UsersConnects::getSteamIDfromIP();
			if($steamID && is_string($steamID)){
				$this->redirect(Yii::app()->createUrl('vip-guest/step/2/?steamid='.UsersConnects::getSteamId64($steamID)));			
			}
		}
		
		$data = array();
		$data['model'] = new PaymentLog;

		if(Yii::app()->request->getQuery('steamid')){
			$data['model']->steam_id = UsersConnects::getSteamId32(Yii::app()->request->getQuery('steamid'));
		}
		
		$this->render('steamID', $data);
	}	

	private function checkProfile(){	
		$steamID = Yii::app()->request->getQuery('steamid');
		$data['steamID'] = $steamID;
		$data['nextUlr'] = Yii::app()->createUrl('vip-guest/step/3/?steamid='.$steamID);
		$data['prevUlr'] = Yii::app()->createUrl('vip-guest/step/1/?steamid='.$steamID);
		
		$this->render('profile', $data);
	}	
	
	private function selectPayment(){
		$data = array();
		
		$steamID = Yii::app()->request->getQuery('steamid');
		$data['steamID'] = $steamID;	
		$payments = Yii::app()->db->createCommand()
			->select('name, settings')
			->from('paysystems')	
			->where('active= :active ', array(':active'=> 1))
			->queryAll();			
		$data['payments'] = array();
		foreach($payments as $payment){
			if(!$payment['settings']){
				continue;
			}
			$settings = json_decode($payment['settings']);
			$data['payments'][$payment['name']] = $settings;		
		}	

		$data['prevUlr'] = Yii::app()->createUrl('vip-guest/step/2/?steamid='.$steamID);		
		
		$this->render('payment', $data);
	}
	
	private function paySms(){
		Yii::import("application.extensions.SMSBill.SMSBill");
		$data = array(
			'error' => '',
			'smsForm' => '',		
		);
				
		$steamID = Yii::app()->request->getQuery('steamid');
		
		$smsbill = new SMSBill();
		switch(Yii::app()->language){
			case 'ru' : $smsbill->setServiceId(11941);	break;
			default :  $smsbill->setServiceId(11918); //изменить на свое ID доступное в Личном кабинете
		}
		
		$smsbill->useEncoding('utf-8');
		$smsbill->useHeader('no');
		$smsbill->useJQuery('yes');
		$smsbill->useLang(Yii::app()->language);
		$smsbill->useCss('http://'.Yii::app()->params['domain'].'/css/payment.css');
		
		$smsbill_password = Yii::app()->request->getPost('smsbill_password');

		if ($smsbill_password) {
			if (!$smsbill->checkPassword($smsbill_password)) {
				$data['error'] =  Yii::t('vip', 'Password is incorrect, go back and try again');
			}else{ 
				//введен верный пароль
				//выполняются алгоритмы в случае верного пароля

				if(Yii::app()->request->isPostRequest){
					$steamID = UsersConnects::getSteamId32(Yii::app()->request->getParam('steamid'));					
					
					$user = new Users;
					$userData = $user->getUserGameData($steamID);
					$name = $userData['playerName'];
					

					// добавляем вип
					$vips = new Vips;					
					$vips->addUser($steamID, $name);		
					$vips->saveFile();						
					
					
					
					// сохранияем в базу
					$log = new PaymentLog;
					$log->steam_id = $steamID;
					$log->paysystem = 1;
					$log->service_type = 1;
					$log->date = date('Y-m-d G:i:s');
					$log->ip = Yii::app()->request->userHostAddress;
					$log->save();
					
					$data['name'] = $name;
					$data['steamID'] = $name;
					$data['payment'] = 'SMS';	
					
					$vip = $vips->getInfo($steamID);;
					
					switch(Yii::app()->language){
						case 'ru' : $data['vipEnd'] = date('d.m.Y | G:i', $vip['expires']);	break;
						default :  $data['vipEnd'] = date('d/m/Y | g.i a', $vip['expires']); 
					}				
					
					
					
					$this->showSuccess($data);	
					die;
				}else{
					$data['error'] =  Yii::t('vip', 'Error, try again');
				}

			}
		}else{
			//показать платежную форму т.к. пароль еще не был введен
			$data['smsForm'] = $smsbill->getForm();
			
			$data['prevUlr'] = Yii::app()->createUrl('vip-guest/step/3/?steamid='.$steamID);
		}	
		$this->render('sms', $data);
	}
	
}