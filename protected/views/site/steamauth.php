<ul>

	<? if($logged) : ?>
		<li><span onclick="location.href='<?=Yii::app()->createUrl('steam/frontend/logout')?>'; "><?=Yii::t('steam', 'Sign out')?></span></li>
		<li><span onclick="location.href='<?=Yii::app()->createUrl('/profile/'.UsersConnects::getSteamId64())?>';"><?=Yii::t('steam', 'My Profile')?></span></li>
	<? else: ?>
		<li><span onclick="window.open('<?=Yii::app()->createUrl('steam/frontend/login')?>','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=480,Height=700');"><?=Yii::t('steam', 'Sign in')?></span></li>
		<li><span onclick="window.open('<?=Yii::app()->createUrl('steam/frontend/registration')?>', '_blank');"><?=Yii::t('steam', 'Registration')?></span></li>
	<? endif; ?>	
</ul>