<?php
class LangSwitcher extends CWidget
{
	public function run() {
		$params = array();
		$params['langs'] = array();
		foreach(Yii::app()->params['translatedLanguages'] as $shortLang => $lang){
			$url = '/';
			if($shortLang == 'en'){
				$domain = 'http://'.Yii::app()->params['domain'];	
			}else{
				$domain =  'http://'.$shortLang.'.'.Yii::app()->params['domain'];
			}
			
			switch(Yii::app()->controller->getRoute()){
				case 'page/frontend/index': break;
				case 'blog/frontend/deteil': break;
				default : $url = Yii::app()->request->url;
			}
			
			$params['langs'][$shortLang]['url'] = $domain.$url;
		}
		$this->render('application.views.site.lang-switcher', $params);
	}	
}
