<?
/* @var $this DefaultController */

//$this->breadcrumbs=array(
//	$this->module->id,
//);
?>
<table class="table table-monitor">
	<? foreach($servers as $id => $server): ?>
		<tr>
			<td><a href="<?=Yii::app()->createUrl('monitor/frontend/deteil',array('server' => $id))?>"><?=$server['name']?></a></td>
			<td><?=$server['address'].':'.$server['port']?></td>
			<? if($server['status']): ?>
				<td>
					<div title="map - <?=$server['map']?>" class="map">
						<?=$server['map']?>
					</div>
				</td>
				<td>
					<div title="players - <?=$server['numberOfPlayers']?>" class="players">
						<?=$server['numberOfPlayers']?>/<?=$server['maxPlayers']?>
					</div>
				</td>
			<? else : ?>
				<td class="server-offline" colspan="3">offline</td>
			<? endif; ?>
		</tr>
	<? endforeach; ?>
</table>			






